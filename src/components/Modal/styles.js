import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = 270;

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        justifyContent: "center"
    },
    appBar: {
        transition: theme.transitions.create(["margin", "width"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "primary"
    },
    // appBarShift: {
    //     width: `calc(100% - ${drawerWidth}px)`,
    //     marginLeft: drawerWidth,
    //     transition: theme.transitions.create(["margin", "width"], {
    //         easing: theme.transitions.easing.easeOut,
    //         duration: theme.transitions.duration.enteringScreen,
    //     }),
    // },
    menuButtonMenu: {
        // marginRight: theme.spacing(2),
        position: "absolute",
        left: "20px",
        top: "10px"
    },
    menuButtonFullscreen: {
        position: "absolute",
        right: "20px",
        top: "10px"
    },
    menuItem: {
        fontWeight: 600
    },
    hide: {
        display: "none",
    }

}))

export default useStyles;