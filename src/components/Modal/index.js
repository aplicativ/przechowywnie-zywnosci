import React, { useContext, setState } from "react";
import { Button } from "@material-ui/core";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";

import ModalContext from "@context/ModalContext";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import printSteps from "@lib/pdf";

import successIcon from "./icons/success.jpg";
import errorIcon from "./icons/error.png";
import warningIcon from "./icons/info.jpg";

import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: "rgba(65,172,38,0.3)",
  },
  backDrop: {},
  icon: {
    textAlign: "center",
    marginBottom: "10px",
    width: "100%",
  },
  iconImg: {
    margin: "0 auto",
  },
  title: {
    textAlign: "center",
    position: "relative",
    maxWidth: "100%",
    marginBottom: "20px",
    color: "#595959",
    fontSize: "25px",
    fontWeight: "600",
    textAlign: "center",
    textTransform: "none",
    wordWrap: "break-word",
  },
  description: {
    color: "#595959",
    textAlign: "left",
    fontSize: "16px",
    marginBottom: "20px",
  },
  buttonsWrap: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    margin: "10px",
  },
}));

const Modal = () => {
  let { modalParams, setModalParams } = useContext(ModalContext);
  const { type, title, text, isOpen } = modalParams;
  const classes = useStyles();

  const handleClose = () => {
    setModalParams({
      ...modalParams,
      isOpen: false,
      type: null,
      title: null,
      text: null,
    });
  };

  return (
    <Dialog
      fullWidth
      open={isOpen}
      onClose={handleClose}
      style={{ zIndex: "999999" }}
      BackdropProps={{
        classes: {
          root: type == "success" ? classes.backDropSuccess : classes.backDrop,
        },
      }}
    >
      <div
        onClick={handleClose}
        style={{ position: "absolute", top: "10px", right: "10px" }}
      >
        <IconButton aria-label="Example">
          <CloseIcon />
        </IconButton>
      </div>
      <DialogContent style={{ padding: "25px" }}>
        {type === "success" && (
          <div className={classes.icon}>
            <img
              className={classes.iconImg}
              src={successIcon}
              alt=""
              crossOrigin="anonymous"
            />
          </div>
        )}
        {type === "error" && (
          <div className={classes.icon}>
            <img
              className={classes.iconImg}
              src={errorIcon}
              alt=""
              crossOrigin="anonymous"
            />
          </div>
        )}
        {type === "warning" && (
          <div className={classes.icon}>
            <img
              className={classes.iconImg}
              src={warningIcon}
              alt=""
              crossOrigin="anonymous"
            />
          </div>
        )}

        {title && <div className={classes.title}>{title}</div>}
        {text && <p className={classes.description}>{text}</p>}

        {type === "error" && (
          <ul>
            <li>
              {" "}
              <a
                className="link"
                aria-label="Przejdź do infografiki"
                target="_blank"
                href="https://edytor.zpe.gov.pl/a/DlrW4DK48"
              >
                Przejdź do infografiki
              </a>
            </li>
          </ul>
        )}

        <div className={classes.buttonsWrap}>
          {type === "error" && (
            <>
              <Button
                variant="contained"
                className={classes.button}
                onClick={() => printSteps()}
              >
                Zapisz listę kroków
              </Button>
              <Button
                onClick={handleClose}
                variant="contained"
                className={classes.button}
                style={{ backgroundColor: "#1979c2", color: "#FFFFFF" }}
              >
                Spróbuj jeszcze raz
              </Button>
            </>
          )}

          {type === "success" && (
            <>
              <Button
                onClick={handleClose}
                variant="contained"
                className={classes.button}
                style={{ backgroundColor: "#357a38", color: "#FFFFFF" }}
              >
                Wróć
              </Button>
              <Button
                onClick={printSteps}
                variant="contained"
                className={classes.button}
                color="primary"
              >
                Zapisz listę kroków
              </Button>
            </>
          )}

          {type === "warning" && (
            <Button
              onClick={handleClose}
              variant="contained"
              className={classes.button}
              style={{ backgroundColor: "#1979c2", color: "#FFFFFF" }}
            >
              Powrót
            </Button>
          )}
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default Modal;
