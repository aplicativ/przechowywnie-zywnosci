import React, { useContext } from "react";
import { Button } from "@material-ui/core";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";

import ModalContext from "@context/ModalContext";
import DataContext from "@context/DataContext";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

import AudioPlayer from "../AudioPlayer";

import instrukcja from "./../../sounds/instrukcja.mp3";

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: "rgba(65,172,38,0.3)",
  },
  backDrop: {},
  icon: {
    textAlign: "center",
    marginBottom: "10px",
    width: "100%",
  },
  iconImg: {
    margin: "0 auto",
  },
  title: {
    textAlign: "center",
    position: "relative",
    maxWidth: "100%",
    marginBottom: "20px",
    color: "#595959",
    fontSize: "25px",
    fontWeight: "600",
    textAlign: "center",
    textTransform: "none",
    wordWrap: "break-word",
  },
  description: {
    color: "#595959",
    fontWeight: "600",
    textAlign: "center",
    fontSize: "16px",
    marginBottom: "20px",
  },
  buttonsWrap: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    margin: "10px",
  },
}));

const Instruction = () => {
  const { isInstructionOpen, setIsInstructionOpen } = useContext(ModalContext);
  const { level } = useContext(DataContext);
  const classes = useStyles();

  const handleClose = () => {
    setIsInstructionOpen(false);
  };

  return (
    <>
      <Dialog fullWidth={true} maxWidth={"lg"} open={isInstructionOpen} onClose={handleClose} style={{ zIndex: "999999" }}>
        <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
          <h3 style={{ marginBottom: "20px" }}>
            Instrukcja obsługi do programu ćwiczeniowego „Rozmieszczanie surowców żywnościowych oraz wyrobów gotowych w magazynach”
          </h3>
          <div style={{ position: "absolute", top: "15px", right: "15px" }}>
            <IconButton onClick={() => setIsInstructionOpen(false)} aria-label="Example">
              <CloseIcon />
            </IconButton>
          </div>
          <article className={classes.instruction}>
            <p>Zapoznaj się z poleceniem.</p>
            <p>
            Dla wygody korzystania z programu kliknij ikonę trybu pełnoekranowego. Umożliwia ona przeglądarce zajęcie całego ekranu.
            </p>
            <p>
            W celu odsłuchania treści zawartych w programie wybierz ikonę „Odsłuchaj”. W dolnym menu znajdziesz również ikony. Po kliknięciu na ikonę będziesz mógł zapoznać się z innymi materiałami <br /> z e-materiału.
            </p>
            <p>Jeśli chcesz zapisać postępy swojej pracy użyj ikony „Pobierz listę kroków”. <br/>
            Plik zapisz na dysku komputera.
            </p>
  

            Program umożliwia również:
            <ul>
              <li>zapisanie całej swojej pracy - ikona „Zapisz efekty pracy”,</li>
              <li>wykonanie zrzutu ekranu - ikona „Zrzut ekranu”</li>
              <li>ponowne wykonanie ćwiczenia - ikona „Spróbuj ponownie"</li>
            </ul>

            <p>Po wykonaniu zadania otrzymasz informację zwrotną.</p>


            W przypadku korzystania wyłącznie z klawiatury należy użyć poniższych klawiszy:
            <ul>
              <li>Tab - poruszanie się do przodu po elementach</li>

              <li>Shift + Tab - poruszanie się do tyłu po elementach</li>

              <li>Spacja - podnoszenie i upuszczanie elementów</li>

              <li>Escape - anulowanie przeciągania elementu</li>

              <li>Strzałki - przenoszenie elementów do sąsiadujących stref upuszczania</li>
            </ul>

            <AudioPlayer url={instrukcja} label="Instrukcja obsługi do programu ćwiczeniowego" />
          </article>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default Instruction;
