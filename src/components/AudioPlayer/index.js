import React, { useContext, useState } from "react";
import ReactAudioPlayer from "react-audio-player";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import MicIcon from "@mui/icons-material/Mic";
import DataContext from "./../../context/DataContext";

const AudioPlayer = ({ url, label }) => {
  const [isOpen, setIsOpen] = useState(false);
  const {getImagePath} = useContext(DataContext);
  return (
    <>
      <Stack direction="row" spacing={2}>
        {!isOpen && (
          <Button
            style={{ width: "100%", margin: "10px 0" }}
            variant="outlined"
            startIcon={<MicIcon />}
            size="small"
            onClick={() => setIsOpen(true)}
            aria-label={label}
          >
            Odsłuchaj
          </Button>
        )}
      </Stack>
      {isOpen && (
        <ReactAudioPlayer
          style={{ width: "100%", margin: "20px 0" }}
          src={getImagePath(url)}
          controls
          autoPlay
        />
      )}
    </>
  );
};

export default AudioPlayer;
