import React, { useContext, setState } from "react";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { jsPDF } from "jspdf";
import html2canvas from "html2canvas";
import DataContext from "../../context/DataContext";
import storehouses from "@data/storehouse";

import "./style.css";

const listItems = [
  { value: "temp0+2", label: "Temperatura od 0 do + 2 C" },
  { value: "temp0+4", label: "Temperatura od 0 do + 4 C" },
  { value: "temp-2+2", label: "Temperatura od -2 do +2 C" },
  { value: "temp+2+4", label: "Temperatura od +2 do + 4 C" },
  { value: "temp4+5", label: "Temperatura od +2 do + 5 C" },
  { value: "temp+6+15", label: "Temperatura: +6 do +15 C" },
  { value: "temp6-10", label: "Temperatura +6 do +10 C" },
  { value: "temp0+10", label: "Temperatura od 0 do +10 C" },
  { value: "temp4+8", label: "Temperatura +4 do +8 C" },
  { value: "temp10+18", label: "Temperatura +10 do +18" },
  { value: "temp15+18", label: "Temperatura +15 do +18 C" },
  { value: "temp-22-18", label: "Temperatura od -22 do -18 C" },

  { value: "wilg60-80", label: "Wilgotność 60-80%" },
  { value: "wilg50", label: "Wilgotność 50%" },
  { value: "wilg60", label: "Wilgotność 60%" },
  { value: "wilg70-80", label: "Wilgotność 70-80%" },
  { value: "wilg70-90", label: "Wilgotność 70-90%" },
  { value: "wilg80", label: "Wilgotność 80%" },
  { value: "wilg80-85", label: "Wilgotność 80-85%" },
  { value: "wilg80-90", label: "Wilgotność 80-90%" },
  { value: "wilg85-90", label: "Wilgotność 85-90%" },
  { value: "wilg90-93", label: "Wilgotność 90-93%" },

  { value: "osw-sztuczne", label: "Oświetlenie sztuczne" },
  { value: "osw-dop-nat", label: "Dopuszcza się pośrednie oświetlenie naturalne" },
  { value: "osw-all", label: "Oświetlenie naturalne, sztuczne lub mieszane" },
  { value: "osw-jesien-bielony", label: "Magazyn przed jesienią powinien być bielony" },

  ,
  { value: "went-mech", label: "Wentylacja mechaniczna" },
];

const useStyles = makeStyles((theme) => ({
  wrap: {
    fontSize: "30px",
    padding: "10px",
    // position:"absolute",
    // top:"0",
    // left:"0",
    // zIndex:-1
  },
  label: {
    margin: "20px 0 40px 0",
    paddingBottom: "10px",
    borderBottom: "3px solid #1e1c37",
    fontSize: "40px",
  },
  message: {
    color: "white",
    borderRadius: "10px",
    textAlign: "center",
    padding: "5px",
    position: "relative",
    top: "-30px",
  },
  success: {
    backgroundColor: "#28a745",
  },
  error: {
    backgroundColor: "#dc3545",
  },
}));

const storeNames = {
  artykulowsuchych: "Artykułów suchych",
  winiwodek: "Win i wódek",
  ziemniakowwarzywkorzeniowych: "Ziemniaków i warzyw korzeniowych",
  kiszonek: "Kiszonek",
  komorachlodniczamiesa: "Komora chłodnicza mięsa",
  komorachlodniczadrobiu: "Komora chłodnicza drobiu",
  komorarybiowocowmorza: "Komora chłodnicza ryb i owoców morza",
  komoranabialu: "Komora chłodnicza nabiału",
  komorawedlintluszczu: "Komora chłodnicza wędlin i tłuszczów",
  komoraowocow: "Komora chłodnicza owoców i warzyw nietrwałych",
  komorajaj: "Komora chłodnicza jaj",
  komoranapojow: "Komora chłodnicza piw i napojów",
  komoraniskotemperaturowa: " Komora niskotemperaturowa",
};

const Steps = ({ type }) => {
  const classes = useStyles();
  const { state, data, averageColumns, easyColumns, columns } = useContext(DataContext);

  const isValid = { easy: false, average: false }

  const prepareArray = (list) => {
    const data = [];
    list.map((item) => data.push(item.key));
    return data;
  };

  const validator = (arr, target) => target.every((v) => arr.includes(v));


  const checker = (obj, validArr) => {
    const arr = prepareArray(obj.list);

    return validator(arr, validArr);
  };


  let {
    artykulowsuchych,
    winiwodek,
    ziemniakowwarzywkorzeniowych,
    kiszonek,
    komorachlodniczamiesa,
    komorachlodniczadrobiu,
    komorarybiowocowmorza,
    komoranabialu,
    komorawedlintluszczu,
    komoraowocow,
    komorajaj,
    komoranapojow,
    komoraniskotemperaturowa,
  } = easyColumns;

  if (
    artykulowsuchych.list.length != 14 ||
    !checker(artykulowsuchych, [
      "maka",
      "kasza",
      "cukier",
      "sol",
      "przyprawy",
      "makaron",
      "pieczywo",
      "ryz",
      "dzemy",
      "kawa",
      "herbata",
      "ciasto",
      "ciasteczka",
      "babeczki",
    ])
  ) {
  } else if (winiwodek.list.length != 5 || !checker(winiwodek, ["winaczerwone", "winarozowe", "winabiale", "wodka", "whisky"])) {
  } else if (
    ziemniakowwarzywkorzeniowych.list.length != 7 ||
    !checker(ziemniakowwarzywkorzeniowych, ["ziemniaki", "buraki", "pietruszka", "seler", "czosnek", "cebula"])
  ) {
  } else if (
    kiszonek.list.length != 7 ||
    !checker(kiszonek, ["kapustakiszona", "sledzie", "ogorkikiszone", "kiszonamarchewka", "zupa", "sos", "golabki"])
  ) {
    ;
  } else if (
    komorachlodniczamiesa.list.length != 6 ||
    !checker(komorachlodniczamiesa, ["watrobkawieprzowa", "pluckawolowe", "zeberka", "lopatka", "nogi", "mostek"])
  ) {

  } else if (komorachlodniczadrobiu.list.length != 3 || !checker(komorachlodniczadrobiu, ["skrzydelka", "szyja", "watrabkadrobiowa"])) {

  } else if (komorarybiowocowmorza.list.length != 4 || !checker(komorarybiowocowmorza, ["losos", "dorsz", "krewetki", "kalmary"])) {

  } else if (
    komoranabialu.list.length != 7 ||
    !checker(komoranabialu, ["mlekoswieze", "mlekouht", "smietana", "jugurt", "maslanka", "twarog", "kefir"])
  ) {
  } else if (
    komorawedlintluszczu.list.length != 10 ||
    !checker(komorawedlintluszczu, [
      "olejroslinny",
      "oliwazoliwek",
      "olejlniany",
      "smalec",
      "olejkokosowy",
      "wedzonki",
      "kielbasy",
      "boczek",
      "szynka",
      "kaszanka",
    ])
  ) {
  } else if (
    komoraowocow.list.length != 19 ||
    !checker(komoraowocow, [
      "pomidory",
      "ogorki",
      "cukinie",
      "baklazany",
      "papryka",
      "dynia",
      "salata",
      "kalafior",
      "imbir",
      "jablka",
      "gruszki",
      "banany",
      "pomarancze",
      "grejpfruty",
      "sliwki",
      "brzoskwinie",
      "ananasy",
      "truskawki",
      "wisnie",
    ])
  ) {
  } else if (komorajaj.list.length != 3 || !checker(komorajaj, ["jaja", "masajajeczna", "pastajajecza"])) {
  } else if (komoranapojow.list.length != 3 || !checker(komoranapojow, ["soki", "napojegazowane", "woda"])) {
  } else if (
    komoraniskotemperaturowa.list.length != 5 ||
    !checker(komoraniskotemperaturowa, ["mrozonewarzywa", "lody", "mrozonepierogi", "mrozoneknedle", "mrozoneowoce"])
  ) {
  } else {
    isValid.easy = true;
  }



  let {
    magazynsuchy,
    magazynwin,
    magazynziemniakowwarzyw,
    magazynkiszonek,
    magazynmiesa,
    magazyndrobiu,
    magazynryb,
    magazynnabialu,
    magazynwedlin,
    magazynwarzywchlodnia,
    magazynpiw,
    magazyntemperatura,
    magazynjaj,
  } = averageColumns;

  const isValidName = (list, key) => {
    if (list.length == 1 && list[0].key == key) {
      return true;
    }
    return false;
  };

  const isValidFeatures = (keyData) => {
    const storehouse = storehouses[keyData].validFeatures;
    const featureData = data[keyData];
    return validator(storehouse, featureData) ? true : false;
  };

  if (!isValidName(magazynsuchy.list, "magazynsuchy") || !isValidFeatures("magazynsuchy")) {
  } else if (!isValidName(magazynwin.list, "magazynwin") || !isValidFeatures("magazynwin")) {
  } else if (!isValidName(magazynziemniakowwarzyw.list, "magazynziemniakowwarzyw") || !isValidFeatures("magazynziemniakowwarzyw")) {
  } else if (!isValidName(magazynkiszonek.list, "magazynkiszonek") || !isValidFeatures("magazynkiszonek")) {
  } else if (!isValidName(magazynmiesa.list, "magazynmiesa") || !isValidFeatures("magazynmiesa")) {
  } else if (!isValidName(magazyndrobiu.list, "magazyndrobiu") || !isValidFeatures("magazyndrobiu")) {
  } else if (!isValidName(magazynryb.list, "magazynryb") || !isValidFeatures("magazynryb")) {
  } else if (!isValidName(magazynnabialu.list, "magazynnabialu") || !isValidFeatures("magazynnabialu")) {
  } else if (!isValidName(magazynwedlin.list, "magazynwedlin") || !isValidFeatures("magazynwedlin")) {
  } else if (!isValidName(magazynwarzywchlodnia.list, "magazynwarzywchlodnia") || !isValidFeatures("magazynwarzywchlodnia")) {
  } else if (!isValidName(magazynpiw.list, "magazynpiw") || !isValidFeatures("magazynpiw")) {
  } else if (!isValidName(magazyntemperatura.list, "magazyntemperatura") || !isValidFeatures("magazyntemperatura")) {
  } else if (!isValidName(magazynjaj.list, "magazynjaj") || !isValidFeatures("magazynjaj")) {
  } else {
    isValid.average = true;
  }



  return (
    <>
      <div style={{ display: "none", width: "297mm" }} id="pdf-file">
        <div className={classes.wrap}>
          <div className={classes.label}>Poziom łatwy:</div>

          <table className="table-steps">
            <tbody>
            <tr>
              <th>Magazyn</th>
              <th>Produkty</th>
            </tr>
            {Object.keys(easyColumns).map((key) => {
              if (key != "menu") {
                const item = easyColumns[key];
                const list = item.list;
                const listNumber = item.list.length;
                return (
                  <tr key={key}>
                    <td>{storeNames[key]}</td>
                    <td>
                      <div>{Object.keys(list).map((key, index) => `${list[key].name}${index === listNumber - 1 ? "" : ","} `)}</div>
                    </td>
                  </tr>
                );
              }
            })}
            </tbody>
          </table>

          {isValid.easy
            ? (
              <div className={`${classes.message} ${classes.success}`}>
                Zadanie wykonane poprawnie
              </div>
            ) : (
              <div className={`${classes.message} ${classes.error}`}>
                Zadanie wykonane niepoprawnie
              </div>
            )}

          <div className={classes.label}>Poziom trudny:</div>

          <table className="table-steps">
            <tbody>
            <tr>
              <th>Magazyn</th>
              <th>Zaznaczone</th>
            </tr>

            {Object.keys(averageColumns).map((key) => {
              if (key != "menu") {
                const item = averageColumns[key];
                const list = item.list;
                const listNumber = item.list.length;
                const featuresArray = state["average"][key];
                const listFeatures = featuresArray.length;
                if (listNumber > 0) {
                  return (
                    <tr key={key}>
                      <td>{Object.keys(list).map((key, index) => list[key].name)}</td>
                      <td>
                        {featuresArray.map((itemKey, index) => {
                          const itemToDisplay = listItems.filter((item) => item.value === itemKey);
                          if (itemToDisplay.length === 1) {
                            return `${itemToDisplay[0].label}${index === listFeatures - 1 ? "" : ","} `;
                          }
                        })}
                      </td>
                    </tr>
                  );
                }
              }
            })}
            </tbody>
          </table>
          {isValid.average
            ? (
              <div className={`${classes.message} ${classes.success}`}>
                Zadanie wykonane poprawnie
              </div>
            ) : (
              <div className={`${classes.message} ${classes.error}`}>
                Zadanie wykonane niepoprawnie
              </div>
            )}
        </div>
      </div>
    </>
  );
};

export default Steps;
