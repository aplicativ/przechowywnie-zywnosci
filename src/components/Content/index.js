import React, { useContext, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import RootRef from '@material-ui/core/RootRef';
import Select from 'react-select';

import DataContext from '@context/DataContext';
import ModalContext from '@context/ModalContext';

import DragDropItem from '@components/DragDropTransfer/subcomponents/item';
import DragDropItemEasy from '@components/DragDropTransfer/subcomponents/itemEasy';

import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Validation from '../Validation';

import storehouses from './../../data/storehouse';

const useStyles = makeStyles({
  content: {
    padding: '10px',
    margin: '30px auto',
    width: '97%',
    // outline: "1px solid  blue",
    maxWidth: '900px',
    minHeight: 1200,
  },

  storehouse: {
    marginBottom: 50,
  },
  storehouseTitle: {
    fontWeight: 600,
    marginBottom: '30px',
    fontSize: '27px',
    textAlign: 'center',
  },
  storehouseList: {
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  storehouseListItem: {
    width: '47%',
    background: '#e8e8e8',
    borderRadius: '5px',
    padding: '10px',
    marginBottom: '20px',
  },
  storehouseListItemTitle: {
    textAlign: 'center',
    padding: '8px',
    fontWeight: 'bold',
  },
  storehouseListItemAverage: {
    width: '47%',
    borderRadius: '5px',
    background: '#e8e8e8',
    padding: '10px',
    marginBottom: '30px',
  },
  storehouseListName: {
    position: 'relative',
    margin: '20px auto 7px auto',
    fontWeight: 'bold',
  },
});

const listItems = [
  { value: 'temp0+2', label: 'Temperatura od 0°C do + 2°C' },
  { value: 'temp0+4', label: 'Temperatura od 0°C do + 4°C' },
  { value: 'temp-2+2', label: 'Temperatura od -2°C do +2°C' },
  { value: 'temp+2+4', label: 'Temperatura od +2°C do + 4°C' },
  { value: 'temp4+5', label: 'Temperatura od +4°C do + 5°C' },
  { value: 'temp+6+15', label: 'Temperatura: +6°C do +15°C' },
  { value: 'temp6-10', label: 'Temperatura +6°C do +10°C' },
  { value: 'temp0+10', label: 'Temperatura od 0°C do +10°C' },
  { value: 'temp4+8', label: 'Temperatura +4°C do +8°C' },
  { value: 'temp10+18', label: 'Temperatura +10°C do +18°C' },
  { value: 'temp15+18', label: 'Temperatura +15°C do +18°C' },
  { value: 'temp-22-18', label: 'Temperatura od -22°C do -18°C' },

  { value: 'wilg60-80', label: 'Wilgotność 60-80%' },
  { value: 'wilg50', label: 'Wilgotność 50%' },
  { value: 'wilg60', label: 'Wilgotność 60%' },
  { value: 'wilg70-80', label: 'Wilgotność 70-80%' },
  { value: 'wilg70-90', label: 'Wilgotność 70-90%' },
  { value: 'wilg80', label: 'Wilgotność 80%' },
  { value: 'wilg80-85', label: 'Wilgotność 80-85%' },
  { value: 'wilg80-90', label: 'Wilgotność 80-90%' },
  { value: 'wilg85-90', label: 'Wilgotność 85-90%' },
  { value: 'wilg90-93', label: 'Wilgotność 90-93%' },

  { value: 'osw-sztuczne', label: 'Oświetlenie sztuczne' },
  {
    value: 'osw-dop-nat',
    label: 'Dopuszcza się pośrednie oświetlenie naturalne',
  },
  { value: 'osw-all', label: 'Oświetlenie naturalne, sztuczne lub mieszane' },
  {
    value: 'osw-jesien-bielony',
    label: 'Magazyn przed jesienią powinien być bielony',
  },

  ,
  { value: 'went-mech', label: 'Wentylacja mechaniczna' },
];

const Content = () => {
  const classes = useStyles();
  const { data, getImagePath, level, state, setState, isClickedAverage } =
    useContext(DataContext);
  const { isInstructionOpen, setIsInstructionOpen, setIsRecommendationOpen } =
    useContext(ModalContext);

  const updateData = (obj, key) => {
    const keys = [];
    obj.map((item) => {
      keys.push(item.value);
    });
    setState({ ...state, [level]: { ...state[level], [key]: keys } });
  };

  const renderFeedbackColor = (key, value) => {
    let color;

    const index = value.index;

    if (isClickedAverage) {
      if (
        value.data.value === data[key][index] &&
        storehouses[key].validFeatures.includes(data[key][index])
      ) {
        color = 'green';
      } else {
        color = 'red';
      }
    }

    return color;
  };

  return (
    <>
      <Stack justifyContent="center">
        <Button
          style={{
            width: '97%',
            maxWidth: '700px',
            margin: '20px auto 20px auto',
          }}
          onClick={() => setIsRecommendationOpen(true)}
          variant="outlined"
        >
          Polecenie
        </Button>
      </Stack>
      <div className={classes.content}>
        {level == 'average' && (
          <div className={classes.storehouseList}>
            {Object.keys(storehouses).map((keyData, index) => {
              const item = storehouses[keyData];

              return (
                <div
                  key={keyData + index}
                  className={classes.storehouseListItemAverage}
                >
                  <div style={{ height: '300px', backgroundColor: 'white' }}>
                    <img
                      style={{
                        width: '100%',
                        maxHeight: 300,
                        background: 'white',
                      }}
                      src={getImagePath(item.image)}
                      alt=""
                      crossOrigin="anonymous"
                    />
                  </div>
                  <div className={classes.storehouseListName}>
                    Nazwa magazynu:
                  </div>
                  <DragDropItem keyData={keyData} />

                  <div className={classes.storehouseListName}>Cechy:</div>
                  <Select
                    defaultValue={data[keyData].map((key) => {
                      const arr = listItems.filter(
                        (item) => item.value === key
                      );
                      return arr.length > 0
                        ? { value: arr[0].value, label: arr[0].label }
                        : false;
                    })}
                    styles={{
                      multiValueLabel: (baseStyles, state) => ({
                        ...baseStyles,
                        color: renderFeedbackColor(keyData, state),
                      }),
                      control: (baseStyles, state) => ({
                        ...baseStyles,
                      }),
                    }}
                    isMulti={true}
                    options={listItems}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    placeholder="Określ tutaj"
                    style={{ width: '100%' }}
                    onChange={(object) => updateData(object, keyData)}
                  />
                </div>
              );
            })}
          </div>
        )}
        {level == 'easy' && (
          <>
            <div className={classes.storehouse}>
              <div className={classes.storehouseTitle}>
                Magazyn niechłodzony
              </div>
              <div className={classes.storehouseList}>
                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Artykułów suchych
                  </div>
                  <DragDropItemEasy keyData="artykulowsuchych" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Win i wódek
                  </div>
                  <DragDropItemEasy keyData="winiwodek" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Ziemniaków i warzyw korzeniowych
                  </div>
                  <DragDropItemEasy keyData="ziemniakowwarzywkorzeniowych" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Kiszonek
                  </div>
                  <DragDropItemEasy keyData="kiszonek" />
                </div>
              </div>
            </div>

            <div className={classes.storehouse}>
              <div className={classes.storehouseTitle}>Magazyn chłodzony</div>

              <div className={classes.storehouseList}>
                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Komora chłodnicza mięsa
                  </div>
                  <DragDropItemEasy keyData="komorachlodniczamiesa" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Komora chłodnicza drobiu
                  </div>
                  <DragDropItemEasy keyData="komorachlodniczadrobiu" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Komora chłodnicza ryb i owoców morza
                  </div>
                  <DragDropItemEasy keyData="komorarybiowocowmorza" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Komora chłodnicza nabiału
                  </div>
                  <DragDropItemEasy keyData="komoranabialu" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Komora chłodnicza wędlin i tłuszczów
                  </div>
                  <DragDropItemEasy keyData="komorawedlintluszczu" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Komora chłodnicza owoców i warzyw nietrwałych
                  </div>
                  <DragDropItemEasy keyData="komoraowocow" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Komora chłodnicza jaj
                  </div>
                  <DragDropItemEasy keyData="komorajaj" />
                </div>

                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Komora chłodnicza piw i napojów
                  </div>
                  <DragDropItemEasy keyData="komoranapojow" />
                </div>
                <div className={classes.storehouseListItem}>
                  <div className={classes.storehouseListItemTitle}>
                    Komora niskotemperaturowa
                  </div>
                  <DragDropItemEasy keyData="komoraniskotemperaturowa" />
                </div>
              </div>
            </div>
          </>
        )}
        <Validation />
      </div>
    </>
  );
};

export default Content;
