import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataContex from "../../context/DataContext";
import { Button } from "@material-ui/core";

import message from "@lib/message";
import dataHelper from "@lib/data";

import storehouses from "@data/storehouse";
import initialState from "@data/initialState";
import ModalContext from "../../context/ModalContext";

import Tooltip from "@mui/material/Tooltip";

//informacja zwrotna pozytywna - uzupełniamy tekstem od redaktora
let successInfo =
  "Ćwiczenie zostało rozwiązane poprawnie. Prawidłowo zostały dobrane magazyny do kategorii produktów. Świetnie!";
//informacja zwrotna negatywna - uzupełniamy tekstem od redaktora
let errorInfo =
  "Jeśli miałeś trudności z wykonaniem zadania wróć do Infografiki, a następnie spróbuj wykonać je jeszcze raz. Zwróć uwagę jaki wybrałeś produkt i zastanów się, w którym magazynie powinien być przechowywany. Następnym razem na pewno się uda!";

let successInfoAverage =
  "Ćwiczenie zostało rozwiązane poprawnie. Prawidłowo zostały ustawione parametry panujące w danych magazynach. Świetnie!";
//informacja zwrotna negatywna - uzupełniamy tekstem od redaktora
let errorInfoAverage =
  "Jeśli miałeś trudności z wykonaniem zadania wróć do Infografiki, a następnie spróbuj wykonać je jeszcze raz. Zwróć uwagę jakie produkty są przechowywane w danym magazynie. Następnym razem na pewno się uda!";

const useStyles = makeStyles((theme) => ({
  wrap: {
    marginTop: "10px",
    paddingBottom: "320px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  span: {
    fontSize: "12px",
  },
}));

const Validation = () => {
  const classes = useStyles();
  const { data, level, columns, state, setIsClicked, setIsClickedAverage } = useContext(DataContex);
  let { setModalParams } = useContext(ModalContext);
  const initModal = ({ type, title, text }) => {
    setModalParams({ type, title, text, isOpen: true });
  };

  const prepareArray = (list) => {
    const data = [];
    list.map((item) => data.push(item.key));
    return data;
  };

  const validator = (arr, target) => target.every((v) => arr.includes(v));

  const checker = (obj, validArr) => {
    const arr = prepareArray(obj.list);

    return validator(arr, validArr);
  };

  const isValid = () => {
    let isValid = false;

    if (level === "easy") {
      const {
        artykulowsuchych,
        winiwodek,
        ziemniakowwarzywkorzeniowych,
        kiszonek,
        komorachlodniczamiesa,
        komorachlodniczadrobiu,
        komorarybiowocowmorza,
        komoranabialu,
        komorawedlintluszczu,
        komoraowocow,
        komorajaj,
        komoranapojow,
        komoraniskotemperaturowa,
      } = columns;

      if (
        winiwodek.list.length === 0 &&
        ziemniakowwarzywkorzeniowych.list.length === 0 &&
        kiszonek.list.length === 0 &&
        komorachlodniczamiesa.list.length === 0 &&
        komorachlodniczadrobiu.list.length === 0 &&
        komorarybiowocowmorza.list.length === 0 &&
        komoranabialu.list.length === 0 &&
        komorawedlintluszczu.list.length === 0 &&
        komoraowocow.list.length === 0 &&
        komorajaj.list.length === 0 &&
        komoranapojow.list.length === 0 &&
        komoraniskotemperaturowa.list.length === 0
      ) {
        initModal({
          type: "warning",
          title: "Niestety...",
          text: "Niestety, nie udzieliłeś żadnej odpowiedzi i nie przyporządkowałeś żadnej nazwy magazynu żywnościowego do rysunków znajdujących się obok, a także nie ustawiłeś żadnych parametrów panujących w danym magazynie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu dowiesz się w jakich magazynach żywnościowych należy przechowywać konkretne produkty spożywcze i następnym razem poprawnie rozwiążesz zadanie. Powodzenia!",
        });
      } else if (
        winiwodek.list.length === 0 ||
        ziemniakowwarzywkorzeniowych.list.length === 0 ||
        (kiszonek.list.length === 0 &&
          komorachlodniczamiesa.list.length === 0) ||
        komorachlodniczadrobiu.list.length === 0 ||
        komorarybiowocowmorza.list.length === 0 ||
        komoranabialu.list.length === 0 ||
        komorawedlintluszczu.list.length === 0 ||
        komoraowocow.list.length === 0 ||
        komorajaj.list.length === 0 ||
        komoranapojow.list.length === 0 ||
        komoraniskotemperaturowa.list.length === 0
      ) {
        initModal({
          type: "warning",
          title: "Niestety...",
          text: "Niestety, nie wszystko poszło dobrze. Nie przyporządkowałeś wszystkich produktów spożywczych do odpowiednich magazynów żywnościowych. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu dowiesz się w jakich magazynach żywnościowych należy przechowywać konkretne produkty spożywcze i następnym razem poprawnie rozwiążesz zadanie. Powodzenia!",
        });

        // initModal({ type: "warning", text: "Niestety, nie udzieliłeś żadnej odpowiedzi i nie przyporządkowałeś żadnej nazwy magazynu żywnościowego do rysunków znajdujących się obok, a także nie ustawiłeś żadnych parametrów panujących w danym magazynie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu dowiesz się w jakich magazynach żywnościowych należy przechowywać konkretne produkty spożywcze i następnym razem poprawnie rozwiążesz zadanie. Powodzenia!" });
      } else {
        if (
          artykulowsuchych.list.length != 14 ||
          !checker(artykulowsuchych, [
            "maka",
            "kasza",
            "cukier",
            "sol",
            "przyprawy",
            "makaron",
            "pieczywo",
            "ryz",
            "dzemy",
            "kawa",
            "herbata",
            "ciasto",
            "ciasteczka",
            "babeczki",
          ])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          winiwodek.list.length != 5 ||
          !checker(winiwodek, [
            "winaczerwone",
            "winabiale",
            "wodka",
            "whisky",
            "wodkalikier",
          ])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          ziemniakowwarzywkorzeniowych.list.length != 4 ||
          !checker(ziemniakowwarzywkorzeniowych, [
            "ziemniaki",
            "buraki",
            "czosnek",
            "cebula",
          ])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
        } else if (
          kiszonek.list.length != 4 ||
          !checker(kiszonek, ["sledzie", "sos", "golabki", "pesto"])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          komorachlodniczamiesa.list.length != 7 ||
          !checker(komorachlodniczamiesa, [
            "watrobkawieprzowa",
            "zeberka",
            "lopatka",
            "nogi",
            "antrykotwolowy",
            "boczek",
            "szynka"
          ])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          komorachlodniczadrobiu.list.length != 1||
          !checker(komorachlodniczadrobiu, ["tusza"])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          komorarybiowocowmorza.list.length != 4 ||
          !checker(komorarybiowocowmorza, [
            "losos",
            "dorsz",
            "krewetki",
            "kalmary",
          ])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          komoranabialu.list.length != 7 ||
          !checker(komoranabialu, [
            "mlekoswieze",
            "mlekouht",
            "smietana",
            "jugurt",
            "maslanka",
            "twarog",
            "kefir",
          ])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          komorawedlintluszczu.list.length != 8 ||
          !checker(komorawedlintluszczu, [
            "olejroslinny",
            "oliwazoliwek",
            "olejlniany",
            "smalec",
            "olejkokosowy",
            "wedzonki",
            "kielbasy",
            "kaszanka",
          ])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          komoraowocow.list.length != 19 ||
          !checker(komoraowocow, [
            "pomidory",
            "ogorki",
            "cukinie",
            "baklazany",
            "papryka",
            "dynia",
            "salata",
            "kalafior",
            "imbir",
            "jablka",
            "gruszki",
            "banany",
            "pomarancze",
            "sliwki",
            "brzoskwinie",
            "ananasy",
            "truskawki",
            "wisnie",
            "arbuzy",
          ])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          komorajaj.list.length != 2||
          !checker(komorajaj, ["jaja", "masajajeczna"])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          komoranapojow.list.length != 3 ||
          !checker(komoranapojow, ["soki", "napojegazowane", "woda"])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else if (
          komoraniskotemperaturowa.list.length != 5 ||
          !checker(komoraniskotemperaturowa, [
            "mrozonewarzywa",
            "lody",
            "mrozonepierogi",
            "mrozoneknedle",
            "mrozoneowoce",
          ])
        ) {
          initModal({ type: "error", title: "Niestety...", text: errorInfo });
          setIsClicked(true);
        } else {
          initModal({ type: "success", title: "Brawo!", text: successInfo });
        }
      }
    } else if (level === "average") {
      const {
        magazynsuchy,
        magazynwin,
        magazynziemniakowwarzyw,
        magazynkiszonek,
        magazynmiesa,
        magazyndrobiu,
        magazynryb,
        magazynnabialu,
        magazynwedlin,
        magazynwarzywchlodnia,
        magazynpiw,
        magazyntemperatura,
        magazynjaj,
      } = columns;

      const isValidName = (list, key) => {
        if (list.length == 1 && list[0].key == key) {
          return true;
        }
        return false;
      };

      const isValidFeatures = (keyData) => {
        const storehouse = storehouses[keyData].validFeatures;
        const featureData = data[keyData];
        if (
          featureData.length === storehouse.length &&
          validator(featureData, storehouse)
        ) {
          return true;
        }
        return false;
      };

      const isEmptyLists = () => {
        const keys = [
          "magazynsuchy",
          "magazynwin",
          "magazynziemniakowwarzyw",
          "magazynkiszonek",
          "magazynmiesa",
          "magazyndrobiu",
          "magazynryb",
          "magazynnabialu",
          "magazynwedlin",
          "magazynwarzywchlodnia",
          "magazynpiw",
          "magazyntemperatura",
          "magazynjaj",
        ];
        let notEmptyKeys = [];
        keys.map((key) => {
          const featureData = data[key];

          if (featureData && featureData.length > 0) {
            notEmptyKeys.push(key);
          }
        });
        return notEmptyKeys.length > 0 ? false : true;
      };

      if (
        magazynsuchy.list.length === 0 &&
        magazynwin.list.length === 0 &&
        magazynziemniakowwarzyw.list.length === 0 &&
        magazynkiszonek.list.length === 0 &&
        magazynmiesa.list.length === 0 &&
        magazyndrobiu.list.length === 0 &&
        magazynryb.list.length === 0 &&
        magazynnabialu.list.length === 0 &&
        magazynwarzywchlodnia.list.length === 0 &&
        magazynpiw.list.length === 0 &&
        magazyntemperatura.list.length === 0 &&
        magazynjaj.list.length === 0 &&
        isEmptyLists()
      ) {
        initModal({
          type: "warning",
          title: "Niestety...",
          text: "Niestety, nie udzieliłeś żadnej odpowiedzi i nie przyporządkowałeś żadnej nazwy magazynu żywnościowego do rysunków znajdujących się obok, a także nie ustawiłeś żadnych parametrów panujących w danym magazynie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e materiału. Dzięki temu dowiesz się w jakich magazynach żywnościowych należy przechowywać konkretne produkty spożywcze i następnym razem poprawnie rozwiążesz zadanie. Powodzenia!",
        });
      } else if (
        magazynsuchy.list.length === 0 ||
        magazynwin.list.length === 0 ||
        magazynziemniakowwarzyw.list.length === 0 ||
        magazynkiszonek.list.length === 0 ||
        magazynmiesa.list.length === 0 ||
        magazyndrobiu.list.length === 0 ||
        magazynryb.list.length === 0 ||
        magazynnabialu.list.length === 0 ||
        magazynwarzywchlodnia.list.length === 0 ||
        magazynpiw.list.length === 0 ||
        magazyntemperatura.list.length === 0 ||
        magazynjaj.list.length === 0 ||
        isEmptyLists()
      ) {
        initModal({
          type: "warning",
          title: "Niestety...",
          text: "Niestety, nie wszystko poszło dobrze. Nie przyporządkowałeś wszystkich nazw magazynów żywnościowych do rysunków znajdujących się obok, a także nie ustawiłeś wszystkich parametrów panujących w danym magazynie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu dowiesz się w jakich magazynach żywnościowych należy przechowywać konkretne produkty spożywcze i następnym razem poprawnie rozwiążesz zadanie. Powodzenia!",
        });
      } else {
        if (
          !isValidName(magazynsuchy.list, "magazynsuchy") ||
          !isValidFeatures("magazynsuchy")
        ) {
        
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)

        } else if (
          !isValidName(magazynwin.list, "magazynwin") ||
          !isValidFeatures("magazynwin")
        ) {
     
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)

        } else if (
          !isValidName(
            magazynziemniakowwarzyw.list,
            "magazynziemniakowwarzyw"
          ) ||
          !isValidFeatures("magazynziemniakowwarzyw")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)
     
        } else if (
          !isValidName(magazynkiszonek.list, "magazynkiszonek") ||
          !isValidFeatures("magazynkiszonek")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)
   
        } else if (
          !isValidName(magazynmiesa.list, "magazynmiesa") ||
          !isValidFeatures("magazynmiesa")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)

        } else if (
          !isValidName(magazyndrobiu.list, "magazyndrobiu") ||
          !isValidFeatures("magazyndrobiu")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)
     
        } else if (
          !isValidName(magazynryb.list, "magazynryb") ||
          !isValidFeatures("magazynryb")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)

        } else if (
          !isValidName(magazynnabialu.list, "magazynnabialu") ||
          !isValidFeatures("magazynnabialu")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)
       
        } else if (
          !isValidName(magazynwedlin.list, "magazynwedlin") ||
          !isValidFeatures("magazynwedlin")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)
      
        } else if (
          !isValidName(magazynwarzywchlodnia.list, "magazynwarzywchlodnia") ||
          !isValidFeatures("magazynwarzywchlodnia")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)
        } else if (
          !isValidName(magazynpiw.list, "magazynpiw") ||
          !isValidFeatures("magazynpiw")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)
  
        } else if (
          !isValidName(magazyntemperatura.list, "magazyntemperatura") ||
          !isValidFeatures("magazyntemperatura")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)
   
        } else if (
          !isValidName(magazynjaj.list, "magazynjaj") ||
          !isValidFeatures("magazynjaj")
        ) {
          initModal({
            type: "error",
            title: "Niestety...",
            text: errorInfoAverage,
          });
          setIsClickedAverage(true)

        } else {
          isValid = true;
          initModal({
            type: "success",
            title: "Brawo!",
            text: successInfoAverage,
          });
          setIsClickedAverage(true)
        }
      }
    }
    return isValid;
  };

  const onClick = () => {
    isValid();

  };

  return (
    <div className={classes.wrap}>
      <Tooltip
        title={
          <span className={classes.span}>
            Kliknij, aby sprawdzić, czy dokonałeś poprawnego wyboru
          </span>
        }
        arrow={true}
        placement={"top"}
      >
        <Button onClick={onClick} variant="contained" color="primary">
          Sprawdź
        </Button>
      </Tooltip>
    </div>
  );
};

export default Validation;
