import React, { useContext, useState, useEffect } from "react";
import { Button } from "@material-ui/core";
import clsx from "clsx";


import ModalContext from "@context/ModalContext";


import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

import useStyles from "./styles.js";


const Modal = () => {
    const classes = useStyles();
    let { alertParams , setAlertParams } = useContext(ModalContext);
    const { type, title, text, isOpen } = alertParams ;

    useEffect(() => {
        if (isOpen) {
            setTimeout(() => {
                setAlertParams({ ...alertParams , isOpen: false });
            }, [4000])
        }

    }, [isOpen]);


    return (
        <div
            className={clsx(`${classes.modal} ${classes.animatedItem}`, {
                [classes.animatedItemExiting]: !isOpen
            })}
        >
            <Stack sx={{ width: '100%' }} spacing={2}>
                {type == "error" && (
                    <Alert variant="filled" severity="error">
                        {text}
                    </Alert>
                )}
                {type == "warning" && (
                    <Alert variant="filled" severity="warning">
                        {text}
                    </Alert>

                )}
                {type == "info" && (
                    <Alert variant="filled" severity="info">
                        {text}
                    </Alert>
                )}
                {type == "success" && (
                    <Alert variant="filled" severity="success">
                        {text}
                    </Alert>
                )}
            </Stack>
        </div>
    )
};

export default Modal;

