import React, { useContext } from 'react';
import clsx from 'clsx';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import DataContex from '@context/DataContext';
import useStyles from './styles';

import CustomTooltip from '@components/CustomTooltip';

import Tooltip from '@material-ui/core/Tooltip';

import FullscreenIn from '@material-ui/icons/Fullscreen';
import FullscreenExit from '@material-ui/icons/FullscreenExit';

const tabProps = (index) => {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
};

const Header = ({ isOpenNavigation, setIsOpenNavigation }) => {
  const classes = useStyles();
  const { setState, level, state, isFullscreen, setIsFullscreen } =
    useContext(DataContex);
  const handleLevelChange = (event, newValue) => {
    setState({ ...state, level: newValue });
  };
  const handleChangeFullscreen = () => {
    setIsFullscreen(!isFullscreen);
  };

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: isOpenNavigation,
      })}
    >
      <Toolbar className={classes.toolbar}>
        <Tabs
          TabIndicatorProps={{
            style: {
              backgroundColor: '#D97D54',
            },
          }}
          value={level}
          onChange={handleLevelChange}
          aria-label=""
        >
          <Tab
            className={classes.menuItem}
            value="easy"
            label="Łatwy"
            {...tabProps(0)}
          />
          <Tab
            className={classes.menuItem}
            value="average"
            label="Trudny"
            {...tabProps(1)}
          />
          {/* <Tab className={classes.menuItem} value="hard" label="Trudny" {...tabProps(2)} /> */}
        </Tabs>
      </Toolbar>
      <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={() => setIsOpenNavigation(!isOpenNavigation)}
        edge="start"
        className={clsx(
          classes.menuButtonMenu,
          isOpenNavigation && classes.hide
        )}
      >
        <MenuIcon style={{ color: 'black' }} />
      </IconButton>
      <Tooltip
        placement="bottom-start"
        arrow={true}
        title={
          isFullscreen ? (
            <span style={{ fontSize: '12px' }}>Wyłącz tryb pełnoekranowy</span>
          ) : (
            <span style={{ fontSize: '12px' }}>Tryb pełnoekranowy</span>
          )
        }
      >
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleChangeFullscreen}
          edge="start"
          className={clsx(classes.menuButtonFullscreen)}
        >
          {isFullscreen ? (
            <FullscreenExit style={{ color: 'black' }} />
          ) : (
            <FullscreenIn style={{ color: 'black' }} />
          )}
        </IconButton>
      </Tooltip>
    </AppBar>
  );
};

export default Header;
