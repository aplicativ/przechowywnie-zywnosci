import React, { useState, useContext } from 'react';

// Components and mui
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';

import magazynSuchy from '../../images/magazyn-artykułów-suchych.png';
import magazynDrob from '../../images/drob.png';
import magazynKiszonki from '../../images/kiszonki.png';
import magazynMieso from '../../images/komora-chlodnicza-mieso.png';
import magazynOwoceWarzywa from '../../images/komora-chłodnicza-owocow-i-warzyw-nietrwalych.png';
import magazynPwio from '../../images/magazyn-piwa.png';
import magazynNabial from '../../images/komory-chłodnicze-nabial.png';
import magazynJaja from '../../images/magazyn-jaj.png';
import magazynZiemniaki from '../../images/magazyn-na-ziemniaki.png';
import magazynWedlin from '../../images/magazyn-wedlin.png';
import magazynWodki from '../../images/magazyn-win-i-wodek.png';
import magazynRyb from '../../images/ryby.png';
import magazynNiskoTemp from '../../images/magazyn-niskotemp.png';

// Data and context
import ModalContext from '../../context/ModalContext';
import DataContext from '../../context/DataContext';

// Style
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  inner: {
    padding: '30px',
  },
  title: {
    margin: '50px 0 ',
    fontSize: '25px',
    textAlign: 'center',
  },
  description: {
    fontWeight: 600,
  },
  close: {
    position: 'absolute',
    top: '10px',
    right: '10px',
  },
  closeIcon: {
    stroke: '#757575',
    strokeWidth: 2,
  },
  tabLabel: {
    fontSize: '18px',
    width: '210px',
  },
  imgContainer: {
    width: '30%',
    margin: '40px',

    '& img': {
      width: '100%',
    },
  },
});

const KnowledgeBase = () => {
  const [tab, setTab] = useState('1');
  const { isKnowledgeBaseOpen, setIsKnowledgeBaseOpen } =
    useContext(ModalContext);
  const { getImagePath } = useContext(DataContext);
  const classes = useStyles();

  const handleClose = () => {
    setIsKnowledgeBaseOpen(false);
  };

  const handleChange = (event, newValue) => {
    setTab(newValue);
  };

  return (
    <Dialog
      style={{ zIndex: '999999', fontSize: '18px' }}
      maxWidth="lg"
      fullWidth
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={isKnowledgeBaseOpen}
    >
      <DialogContent className={classes.inner}>
        <div className={classes.close}>
          <IconButton onClick={handleClose} aria-label="close" component="span">
            <CloseIcon className={classes.closeIcon} />
          </IconButton>
        </div>
        <div>
          <h4 className={classes.title}>Magazyn niech&#322;odzony</h4>
          <p className={classes.description}>
            <span>Magazyn niech&#322;odzony artyku&#322;&oacute;w suchych</span>
          </p>
          <p>
            <span>
              W magazynie niech&#322;odzonym artyku&#322;&oacute;w suchych
              przechowujemy przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>m&#261;k&#281; pszenn&#261; w opakowaniu papierowym,</span>
            </li>
            <li>
              <span>kasz&#281; w opakowaniu kartonowym,</span>
            </li>
            <li>
              <span>cukier kryszta&#322;,</span>
            </li>
            <li>
              <span>s&oacute;l, </span>
            </li>
            <li>
              <span>przyprawy w opakowaniach jednostkowych,</span>
            </li>
            <li>
              <span>makaron w opakowaniu kartonowym,</span>
            </li>
            <li>
              <span>ry&#380; w opakowaniu papierowym,</span>
            </li>
            <li>
              <span>d&#380;emy w s&#322;oikach,</span>
            </li>
            <li>
              <span>kaw&#281; ziarnist&#261; w opakowaniu foliowym,</span>
            </li>
            <li>
              <span>herbat&#281; sypk&#261; w opakowaniu kartonowym,</span>
            </li>
            <li>
              <span>pieczywo &#347;wie&#380;o pieczone,</span>
            </li>
            <li>
              <span>ciastka kruche &#347;wie&#380;o pieczone,</span>
            </li>
            <li>
              <span>ciasto czekoladowe &#347;wie&#380;o pieczone,</span>
            </li>
            <li>
              <span>babeczki &#347;wie&#380;o pieczone.</span>
            </li>
          </ul>
          <p className={classes.description}>
            <span>Magazyn niech&#322;odzony win i w&oacute;dek</span>
          </p>
          <p>
            <span>
              W magazynie niech&#322;odzonym win i w&oacute;dek przechowujemy
              przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>wina czerwone,</span>
            </li>
            <li>
              <span>wina bia&#322;e,</span>
            </li>
            <li>
              <span>w&oacute;dk&#281; typu whisky,</span>
            </li>
            <li>
              <span>w&oacute;dk&#281; s&#322;odk&#261; typu likier,</span>
            </li>
            <li>
              <span>w&oacute;dk&#281;.</span>
            </li>
          </ul>
          <p className={classes.description}>
            <span>
              Magazyn niech&#322;odzony ziemniak&oacute;w i warzyw korzeniowych
            </span>
          </p>
          <p>
            <span>
              W magazynie niech&#322;odzonym ziemniak&oacute;w i warzyw
              korzeniowych przechowujemy przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>ziemniaki,</span>
            </li>
            <li>
              <span>buraki &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>czosnek &#347;wie&#380;y,</span>
            </li>
            <li>
              <span>cebul&#281; &#347;wie&#380;&#261;.</span>
            </li>
          </ul>
          <p className={classes.description}>
            <span>Magazyn niech&#322;odzony kiszonek</span>
          </p>
          <p>
            <span>
              W magazynie niech&#322;odzonym kiszonek przechowujemy
              przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>szproty marynowane w puszcze,</span>
            </li>
            <li>
              <span>
                sos (wyr&oacute;b garma&#380;eryjny pasteryzowany w
                s&#322;oiku),
              </span>
            </li>
            <li>
              <span>
                pesto (wyr&oacute;b garma&#380;eryjny pasteryzowany w
                s&#322;oiku),
              </span>
            </li>
            <li>
              <span>
                go&#322;&#261;bki (wyr&oacute;b garma&#380;eryjny pasteryzowany
                w s&#322;oiku).
              </span>
            </li>
          </ul>

          <h4 className={classes.title}>Magazyn ch&#322;odzony</h4>

          <p className={classes.description}>
            <span>Komora ch&#322;odnicza mi&#281;sa</span>
          </p>
          <p>
            <span>
              W komorze ch&#322;odniczej mi&#281;sa przechowujemy
              przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>antrykot wo&#322;owy &#347;wie&#380;y,</span>
            </li>
            <li>
              <span>boczek wieprzowy &#347;wie&#380;y,</span>
            </li>
            <li>
              <span>
                &#322;opatk&#281; wieprzow&#261; &#347;wie&#380;&#261;,
              </span>
            </li>
            <li>
              <span>nogi wieprzowe &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>szynk&#281; wieprzow&#261; &#347;wie&#380;&#261;,</span>
            </li>
            <li>
              <span>
                w&#261;trob&#281; ciel&#281;c&#261; &#347;wie&#380;&#261;,
              </span>
            </li>
            <li>
              <span>&#380;eberka wieprzowe &#347;wie&#380;e.</span>
            </li>
          </ul>
          <p className={classes.description}>
            <span>Komora ch&#322;odnicza drobiu</span>
          </p>
          <p>
            <span>
              W komorze ch&#322;odniczej drobiu przechowujemy przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>&#347;wie&#380;&#261; tusz&#281; drobiow&#261;.</span>
            </li>
          </ul>
          <p>
            <span className={classes.description}>
              Komora ch&#322;odnicza ryb i owoc&oacute;w morza
            </span>
          </p>
          <p>
            <span>
              W komorze ch&#322;odniczej ryb i owoc&oacute;w morza przechowujemy
              przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>&#322;ososia &#347;wie&#380;ego,</span>
            </li>
            <li>
              <span>dorsza &#347;wie&#380;ego,</span>
            </li>
            <li>
              <span>krewetki &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>kalmary &#347;wie&#380;e.</span>
            </li>
          </ul>
          <p>
            <span className={classes.description}>
              Komora ch&#322;odnicza nabia&#322;u
            </span>
          </p>
          <p>
            <span>
              W komorze ch&#322;odniczej nabia&#322;u przechowujemy
              przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>jogurt, </span>
            </li>
            <li>
              <span>kefir,</span>
            </li>
            <li>
              <span>ma&#347;lank&#281;,</span>
            </li>
            <li>
              <span>mleko &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>mleko UHT,</span>
            </li>
            <li>
              <span>&#347;mietan&#281;,</span>
            </li>
            <li>
              <span>twar&oacute;g. </span>
            </li>
          </ul>
          <p>
            <span className={classes.description}>
              Komora ch&#322;odnicza w&#281;dlin i t&#322;uszcz&oacute;w
            </span>
          </p>
          <p>
            <span>
              W komorze ch&#322;odniczej w&#281;dlin i t&#322;uszcz&oacute;w
              przechowujemy przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>olej s&#322;onecznikowy,</span>
            </li>
            <li>
              <span>oliw&#281; z oliwek,</span>
            </li>
            <li>
              <span>olej lniany,</span>
            </li>
            <li>
              <span>smalec,</span>
            </li>
            <li>
              <span>olej kokosowy,</span>
            </li>
            <li>
              <span>w&#281;dzonki,</span>
            </li>
            <li>
              <span>kie&#322;basy,</span>
            </li>
            <li>
              <span>kaszank&#281;. </span>
            </li>
          </ul>
          <p className={classes.description}>
            <span>
              Komora ch&#322;odnicza owoc&oacute;w i warzyw nietrwa&#322;ych
            </span>
          </p>
          <p>
            <span>
              W komorze ch&#322;odniczej owoc&oacute;w i warzyw nietrwa&#322;ych
              przechowujemy przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>imbir &#347;wie&#380;y,</span>
            </li>
            <li>
              <span>pomidory &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>og&oacute;rki zielone,</span>
            </li>
            <li>
              <span>cukinie &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>bak&#322;a&#380;any &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>papryk&#281; &#347;wie&#380;&#261;,</span>
            </li>
            <li>
              <span>dyni&#281; &#347;wie&#380;&#261;,</span>
            </li>
            <li>
              <span>sa&#322;at&#281; lodow&#261;,</span>
            </li>
            <li>
              <span>jab&#322;ka &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>gruszki &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>banany &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>pomara&#324;cze &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>&#347;liwki &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>brzoskwinie &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>ananasy &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>arbuzy &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>truskawki &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>wi&#347;nie &#347;wie&#380;e,</span>
            </li>
            <li>
              <span>kalafior &#347;wie&#380;y.</span>
            </li>
          </ul>
          <p className={classes.description}>
            <span>Komora ch&#322;odnicza jaj</span>
          </p>
          <p>
            <span>
              W komorze ch&#322;odniczej jaj przechowujemy przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>jaja kurze,</span>
            </li>
            <li>
              <span>mas&#281; jajeczn&#261; UHT w kartonie.</span>
            </li>
          </ul>
          <p className={classes.description}>
            <span>Komora ch&#322;odnicza piw i napoj&oacute;w</span>
          </p>
          <p>
            <span>
              W komorze ch&#322;odniczej piw i napoj&oacute;w przechowujemy
              przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>soki pasteryzowane w butelkach,</span>
            </li>
            <li>
              <span>napoje gazowane,</span>
            </li>
            <li>
              <span>wod&#281; mineraln&#261; butelkowan&#261;.</span>
            </li>
          </ul>
          <p className={classes.description}>
            <span>Komora niskotemperaturowa</span>
          </p>
          <p>
            <span>
              W komorze niskotemperaturowej przechowujemy przyk&#322;adowo:
            </span>
          </p>
          <ul>
            <li>
              <span>mro&#380;one warzywa, </span>
            </li>
            <li>
              <span>lody,</span>
            </li>
            <li>
              <span>mro&#380;one pierogi,</span>
            </li>
            <li>
              <span>mro&#380;one knedle,</span>
            </li>
            <li>
              <span>mro&#380;one owoce.</span>
            </li>
          </ul>

          <h4 className={classes.title}>Rodzaje magazynów</h4>

          <div style={{ marginLeft: '50px' }}>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynSuchy)} />
              </div>
              <div>
                <p className={classes.description}>Magazyn artykułów suchych</p>
                <p>
                  Temperatura +15 do +18℃ <br />
                  Wilgotność 60% <br />
                  Dopuszcza się pośrednie oświetlenie naturalne <br />
                  Wentylacja mechaniczna{' '}
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynWodki)} />
              </div>
              <div>
                <p className={classes.description}>Magazyn win i wódek</p>
                <p>
                  Temperatura +10 do +18℃
                  <br />
                  Wilgotność 60-80% <br />
                  Oświetlenie sztuczne
                  <br />
                  Wentylacja mechaniczna{' '}
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynZiemniaki)} />
              </div>
              <div>
                <p className={classes.description}>
                  Magazyn ziemniaków i warzyw korzeniowych
                </p>
                <p>
                  Temperatura +6 do +10℃
                  <br />
                  Wilgotność 85-90%
                  <br />
                  Oświetlenie sztuczne
                  <br />
                  Wentylacja mechaniczna <br />
                  Magazyn przed jesienią powinien być bielony
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynKiszonki)} />
              </div>
              <div>
                <p className={classes.description}>Magazyn kiszonek</p>
                <p>
                  Temperatura +6 do +15℃
                  <br />
                  Wilgotność 70-80% <br />
                  Oświetlenie sztuczne
                  <br />
                  Wentylacja mechaniczna{' '}
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynMieso)} />
              </div>
              <div>
                <p className={classes.description}>Komora chłodnicza mięsa</p>
                <p>
                  Temperatura 0 do +4℃
                  <br />
                  Wilgotność 70-90%
                  <br />
                  Oświetlenie sztuczne
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynDrob)} />
              </div>
              <div>
                <p className={classes.description}>Komora chłodnicza drobiu</p>
                <p>
                  Temperatura 0 do +4℃
                  <br />
                  Wilgotność 70-90%
                  <br />
                  Oświetlenie sztuczne
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynRyb)} />
              </div>
              <div>
                <p className={classes.description}>
                  Komora chłodnicza ryb i owoców morza
                </p>
                <p>
                  Temperatura -2 do +2℃
                  <br />
                  Wilgotność 90-93%
                  <br />
                  Oświetlenie sztuczne
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynNabial)} />
              </div>
              <div>
                <p className={classes.description}>Komora chłodnicza nabiału</p>
                <p>
                  Temperatura +2 do +4℃
                  <br />
                  Wilgotność 80-85%
                  <br />
                  Oświetlenie sztuczne
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynWedlin)} />
              </div>
              <div>
                <p className={classes.description}>
                  Komora chłodnicza wędlin i tłuszczów
                </p>
                <p>
                  Temperatura 0 do +2℃
                  <br />
                  Wilgotność 80%
                  <br />
                  Oświetlenie sztuczne
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynOwoceWarzywa)} />
              </div>
              <div>
                <p className={classes.description}>
                  Komora chłodnicza owoców i warzyw nietrwałych
                </p>
                <p>
                  Temperatura +4 do +8℃
                  <br />
                  Wilgotność 80-85%
                  <br />
                  Oświetlenie sztuczne
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynPwio)} />
              </div>
              <div>
                <p className={classes.description}>
                  Komora chłodnicza piw i napojów
                </p>
                <p>
                  Temperatura +4 do +8℃
                  <br />
                  Wilgotność 80-85%
                  <br />
                  Oświetlenie sztuczne
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynNiskoTemp)} />
              </div>
              <div>
                <p className={classes.description}>Komora niskotemperaturowa</p>
                <p>
                  Temperatura -22 do -18℃
                  <br />
                  Wilgotność 80-90%
                  <br />
                  Oświetlenie sztuczne
                </p>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <div className={classes.imgContainer}>
                <img src={getImagePath(magazynJaja)} />
              </div>
              <div>
                <p className={classes.description}>Komora chłodnicza jaj</p>
                <p>
                  Temperatura +4 do +5℃
                  <br />
                  Wilgotność 50%
                  <br />
                  Oświetlenie sztuczne
                </p>
              </div>
            </div>
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default KnowledgeBase;
