import React, { useContext } from 'react';
import RootRef from "@material-ui/core/RootRef";
// import { DragDropContext, Droppable } from "react-beautiful-dnd";

import { makeStyles } from '@material-ui/core/styles'

import productsData from "@data/products/list";


import RadioGroupPrimary from "./subcomponents/RadioGroupPrimary";

import DataContext from "@context/DataContext";
import DragDropItem from "@components/DragDropTransfer/subcomponents/itemMenu";


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },

    formControlLabel: {
        '& span': {
            fontSize: '0.8em',
        },
    },
}))


const Menu = () => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            {/* <RadioGroupPrimary
                items={productsData}
                keyData="product"
                title="Produkty"
                key="test1" /> */}
            {/* <RadioGroupPrimary
                items={typesData}
                keyData="type"
                title="Typ"
                key="test2" />
            <RadioGroupPrimary
                items={fabricsData}
                keyData="fabric"
                title="Tkanina"
                isNested={true}
                key="test3"
            /> */}
            <div style={{ textAlign: "center", fontWeight: 600, padding: "10px 0",backgroundColor: "#FFC445" }}>
                Menu
            </div>
            <DragDropItem keyData="menu" />
        </div>
    )
}

export default Menu;