import React, { useState, useContext, useEffect } from 'react'
import { useSnackbar } from 'notistack'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import DataContex from "@context/DataContext";
import ModalContext from "@context/ModalContext";

import useStyles from './styles'
import { updateCookieState, getCookie } from "@lib/cookies";


const RadioGroupPrimary = ({ title, items, keyData, isNested = false }) => {
    const classes = useStyles();
    const { enqueueSnackbar } = useSnackbar()
    const { data, setState, state, level, appID } = useContext(DataContex);
    const { setModalFunctionalParams } = useContext(ModalContext);
    const [isOpen, setIsOpen] = useState(false);
    let { [keyData]: selectedDefault, bannedFeatures } = data;
    const selected = selectedDefault;

    const sortedItems = !isNested ? items.filter(item => item.level.includes("all") || item.level.includes(level)) : [];


    const initModalFunctional = ({ type, text }) => {
        setModalFunctionalParams({ type, text, isOpen: true });
    }

    useEffect(() => {
        data[keyData]
            ? setIsOpen(true)
            : setIsOpen(false);
    }, [state]);


    const updateData = (value) => {
        const newValue = value.target.value === selected ? null : value.target.value;
        const newState = { ...state, [level]: { ...state[level], [keyData]: newValue } };
        setState(newState);
        // updateCookieState(appID, newState);
    }
    const onClickBanned = () => {
        initModalFunctional({ type: "info", text: "Ten element nie może być użyty w tym pomieszczeniu, wybierz inny element z bazy danych." })
    }
    return (
        <div>
            <Accordion
                expanded={isOpen}
                onClick={()=>setIsOpen(!isOpen)}
            >
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                >
                    <Typography className={classes.heading}>{title}</Typography>
                </AccordionSummary>
                <AccordionDetails className={classes.bannedWrap}>
                    {bannedFeatures.includes(keyData) && (
                        <div onClick={onClickBanned} className={classes.banned}></div>
                    )}

                    {isNested ? (
                        <RadioGroup aria-label="transport" value={selected}>
                            {items.map(section => {
                                const { items, title } = section;
                                const sortedData = items.filter(item => item.level.includes("all") || item.level.includes(level));
                                return (
                                    <>
                                        <Typography className={classes.subitemTitle}>{title}</Typography>
                                        {sortedData.map(item => (
                                            <FormControlLabel
                                                value={item.key}
                                                control={<Radio color="secondary" />}
                                                label={item.name}
                                                key={item.value}
                                                className={classes.formControlLabel}
                                                control={<Radio onClick={updateData} />}
                                            />
                                        ))}
                                    </>
                                )
                            })}
                        </RadioGroup>
                    ) : (
                        <RadioGroup aria-label="transport" value={selected ? selected : ""}>
                            {sortedItems.map(item => (
                                <FormControlLabel
                                    value={item.key}
                                    control={<Radio color="secondary" />}
                                    label={item.name}
                                    key={item.value}
                                    className={classes.formControlLabel}
                                    control={<Radio onClick={updateData} />}
                                />
                            ))}
                        </RadioGroup>
                    )}
                </AccordionDetails>
            </Accordion>
        </div>
    )
}

export default RadioGroupPrimary;