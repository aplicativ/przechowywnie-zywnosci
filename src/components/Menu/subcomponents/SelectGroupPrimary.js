import React, { useContext, useState, useEffect } from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import useStyles from './styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import DataContext from "../../../context/DataContext";
import { updateCookieState, getCookie } from "@lib/cookies";

export default function CheckboxLabels({ title, items, keyData }) {
    const classes = useStyles();
    const { data, setState, state, level } = useContext(DataContext);
    const [isOpen, setIsOpen] = useState(false);
    const updateData = (value) => {

        let { calendarMachine } = data;
        if (!calendarMachine.includes(value)) {
            calendarMachine.push(value);
        } else {
            calendarMachine = calendarMachine.filter(item => item !== value);
        }

        const newState = { ...state, [level]: { ...state[level], [keyData]: calendarMachine } };
        setState(newState);
        // updateCookieState(appID, newState);


    }

    useEffect(() => {
        data[keyData]
            ? setIsOpen(true)
            : setIsOpen(false);
    }, [state]);

    return (
        <Accordion
            expanded={isOpen}
            onClick={() => setIsOpen(!isOpen)}
        >
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header"
            >
                <Typography className={classes.heading}>{title}</Typography>
            </AccordionSummary>
            <AccordionDetails className={classes.bannedWrap}>
                <FormGroup>
                    {items.map(item => (
                        <FormControlLabel
                            control={<Checkbox onClick={(e) => updateData(e.target.value)} />}
                            label={item.name}
                            value={item.key}

                        />
                    ))}
                </FormGroup>
            </AccordionDetails>
        </Accordion>
    );
}