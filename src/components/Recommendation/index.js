import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";

import ModalContext from "@context/ModalContext";
import DataContext from "@context/DataContext";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

import AudioPlayer from "../AudioPlayer";

import polecenie1 from "./../../sounds/polecenie1.mp3";
import polecenie2 from "./../../sounds/polecenie2.mp3";

const useStyles = makeStyles((theme) => ({
  table: {
    width: "100%",
    borderCollapse: "collapse",
  },
  th: {
    border: "1px slid #dddd",
    textAlign: "left",
    padding: "8px",
  },
}));

const Recommendation = () => {
  const classes = useStyles();
  const { isRecommendationOpen, setIsRecommendationOpen } =
    useContext(ModalContext);
  const { level, getImagePath } = useContext(DataContext);

  const handleClose = () => {
    setIsRecommendationOpen(false);
  };

  return (
    <>
      <Dialog
        fullWidth={true}
        open={isRecommendationOpen}
        onClose={handleClose}
        style={{ zIndex: "999999" }}
      >
        <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
          <h3 style={{ marginBottom: "20px" }}>Polecenie</h3>
          <div style={{ position: "absolute", top: "15px", right: "15px" }}>
            <IconButton
              onClick={() => setIsRecommendationOpen(false)}
              aria-label="Zamknij"
            >
              <CloseIcon />
            </IconButton>
          </div>

          <div className={classes.instruction}>
            <p>
              {level === "easy" && (
                <>
                  <div>Wybierz i przeciągnij elementy z menu bocznego.</div>
                  <br />
                  <div>
                    Dobierz wszystkie prawidłowe produkty spożywcze do
                    odpowiednich magazynów żywnościowych.
                  </div>
                  <br />
                  <div>
                    Pamiętaj, że w dolnym menu znajdziesz również ikonę
                    „Infografika”, po kliknięciu której będziesz mógł wrócić do
                    wiedzy o prawidłowym przechowywaniu produktów spożywczych w
                    odpowiednich magazynach. Powodzenia!
                  </div>
                  <br />
                  <AudioPlayer url={polecenie1} label="Polecenie" />
                </>
              )}
              {level === "average" && (
                <>
                  <div>
                    Wybierz i przeciągnij elementy z menu bocznego do komórki
                    „Nazwa magazynu”.
                  </div>
                  <br />
                  <div>
                    Dobierz prawidłowe nazwy magazynów żywnościowych do
                    wszystkich rysunków magazynów.
                  </div>
                  <br />
                  <div>
                    Rozwiń menu w komórce „Cechy” i ustaw odpowiednie parametry
                    panujące w danym magazynie.
                  </div>
                  <br />
                  <div>
                    Pamiętaj, że w dolnym menu znajdziesz również ikonę
                    „Infografika”, po kliknięciu której będziesz mógł wrócić do
                    wiedzy o magazynach żywnościowych, przechowywanych w nich
                    produktach oraz warunkach w nich panujących. Powodzenia!
                  </div>
                  <br />
                  <AudioPlayer url={polecenie2} label="Polecenie" />
                </>
              )}
            </p>
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default Recommendation;
