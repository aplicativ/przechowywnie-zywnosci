import React, { useContext, useState } from "react";
import "./index.css";

const CustomTooltip = ({ children,title }) => {

  return (
    <>
      <div title={title} class="tooltip">{children}</div>
    </>
  );
};

export default CustomTooltip;
