import React, { useContext } from "react";
import RootRef from "@material-ui/core/RootRef";
import { Droppable } from "react-beautiful-dnd";
import DataContext from "@context/DataContext";

import List from "@material-ui/core/List";
import ListItem from "./list";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  list: {
    paddingInlineStart: "0",
    // backgroundColor: "#f4f4f4",
    minHeight: "30px",
    background: "white",
    // minHeight: "100px",
    height:"500px",
    overflowY:"scroll",
    padding: "0",
    outline:"2px solid pink",
  },
  listWrap : {
    height:"500px",
    outline:"2px solid pink",
  },
  listItemList: {
    width: "100%",
  },
  listItemListAverage: {
    width: "100%",
    minHeight: "38px",
    backgroundColor: "white",
  },
}));

const Item = ({ keyData }) => {
  const { columns, level } = useContext(DataContext);
  const { [keyData]: data } = columns;
  const classes = useStyles();

  return (
    <div className={classes.listWrap}>
      <Droppable droppableId={data.id}>
        {(provided) => (
          <RootRef rootRef={provided.innerRef}>
            <List
              className={`
                    ${classes.list} `}
            >
              {data.list.map((itemObject, index) => {
                return <ListItem keyData={keyData} index={index} itemObject={itemObject} />;
              })}
              {provided.placeholder}
            </List>
          </RootRef>
        )}
      </Droppable>
    </div>
  );
};

export default Item;
