import React from "react";

import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import { Draggable } from "react-beautiful-dnd";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  listItem: {
    position: "relative",
    paddingLeft: "2px",
    marginLeft: "2px"
  },
  number: {
    position: "absolute",
    top: "3px",
    left: "0",
    transform: "translateX(-120%)",
    // backgroundColor: "#1e1c37",
    color: "white",
    borderRadius: "50%",
    paddingTop: "4px",
    width: "20px",
    height: "20px",
    display: "flex",
    alignContent: "center",
    justifyContent: "center",
    fontSize: "10px",
    fontWeight: "bold"
  }
}))



const ListItemCustom = ({ itemObject, index, keyData }) => {
  const classes = useStyles();
  return (
    <Draggable draggableId={itemObject.key} key={itemObject.key} index={index}>
      {(provided) => (
        <ListItem
          key={itemObject.key}
          role={undefined}
          dense
          button
          ContainerComponent="li"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}

        >
          <ListItemText
            primary={<div type="body2" className={`${classes.listItem}`}>{itemObject.name}</div>}
            style={{ paddingLeft: "0 !import", paddingRight: "0 !import", }}
          />
          <ListItemSecondaryAction style={{ width: "0" }}>
            {/* <IconButton
              edge="end"
              aria-label="comments"
              question-uid={itemObject.key}
            >
            </IconButton> */}
          </ListItemSecondaryAction>
        </ListItem>
      )}
    </Draggable>
  );
};

export default ListItemCustom;
