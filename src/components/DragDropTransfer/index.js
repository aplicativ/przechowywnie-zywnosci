import React, { useContext } from "react";
import useStyles from './styles'
import { DragDropContext } from "react-beautiful-dnd";
import DataContext from "@context/DataContext";


const DragDropTransfer = ({ children }) => {
    const { columns, setColumns, level } = useContext(DataContext);
    const classes = useStyles();

    const onDragEnd = ({ source, destination }) => {
        // Make sure we have a valid destination
        if (destination === undefined || destination === null) return null;

        // Make sure we're actually moving the item
        if (
            source.droppableId === destination.droppableId &&
            destination.index === source.index
        )
            return null;

        // Set start and end variables
        const start = columns[source.droppableId];
        const end = columns[destination.droppableId];


        // If start is the same as end, we're in the same column
        if (start === end) {
            // Move the item within the list
            // Start by making a new list without the dragged item

            const newList = start.list.filter((_, idx) => idx !== source.index);

            // Then insert the item at the right location
            newList.splice(destination.index, 0, start.list[source.index]);

            // Then create a new copy of the column object
            const newCol = {
                id: start.id,
                list: newList
            };

            // Update the state
            setColumns((state) => ({ ...state, [newCol.id]: newCol }));
            return null;
        } else {
            let newStartList = [];
            let newEndList = [];

            if (level == "easy") {
        
                newStartList = start.list.filter((_, idx) => idx !== source.index);
                newEndList = end.list;
                newEndList.splice(destination.index, 0, start.list[source.index]);
        
            }
            if (level == "average") {

                if (source.droppableId === "menu" && end.id !== "menu") {
                    newEndList = [];
                    newStartList = start.list.filter((_, idx) => idx !== source.index);

                    if (end.list.length > 0) {
                        const currentEndItem = end.list[0];
                        newStartList.push(currentEndItem);
                        newEndList.splice(destination.index, 0, start.list[source.index]);
                    } else {
                        newEndList.splice(destination.index, 0, start.list[source.index]);
                    }

                } else {
                    newStartList = start.list.filter((_, idx) => idx !== source.index);
                    newEndList = end.list;
                    newEndList.splice(destination.index, 0, start.list[source.index]);
                }

            }
            const newStartCol = {
                id: start.id,
                list: newStartList
            };

    
            const newEndCol = {
                id: end.id,
                list: newEndList
            };


            setColumns((state) => ({
                ...state,
                [newStartCol.id]: newStartCol,
                [newEndCol.id]: newEndCol
            }));
            return null;
        }
    };

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            {children}
        </DragDropContext>
    );
};

export default DragDropTransfer;
