import React, { useContext } from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import { Draggable } from 'react-beautiful-dnd';
import { makeStyles } from '@material-ui/core/styles';
import arbuz from '../../../images/products/arbuzy.png';
import DataContext from '@context/DataContext';

const useStyles = makeStyles((theme) => ({
  listItem: {
    position: 'relative',
    paddingLeft: '2px',
    width: '125%',

    marginLeft: '2px',
  },
  number: {
    position: 'absolute',
    top: '3px',
    left: '0',
    transform: 'translateX(-120%)',
    // backgroundColor: "#1e1c37",
    color: 'white',
    borderRadius: '50%',
    paddingTop: '4px',
    width: '20px',
    height: '20px',
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
    fontSize: '10px',
    fontWeight: 'bold',
  },
  error: {
    background: 'red',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    zIndex: 0,
    opacity: 0.2,
  },
  success: {
    background: 'green',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    zIndex: 0,
    opacity: 0.2,
  },
}));

const validData = {
  artykulowsuchych: [
    'maka',
    'kasza',
    'cukier',
    'sol',
    'przyprawy',
    'makaron',
    'pieczywo',
    'ryz',
    'dzemy',
    'kawa',
    'herbata',
    'ciasto',
    'ciasteczka',
    'babeczki',
  ],
  winiwodek: ['winaczerwone', 'winabiale', 'wodka', 'whisky', 'wodkalikier'],
  ziemniakowwarzywkorzeniowych: ['ziemniaki', 'buraki', 'czosnek', 'cebula'],
  kiszonek: ['sledzie', 'sos', 'golabki', 'pesto'],
  komorachlodniczadrobiu: ['tusza'],
  komorarybiowocowmorza: ['losos', 'dorsz', 'krewetki', 'kalmary'],
  komoranabialu: [
    'mlekoswieze',
    'mlekouht',
    'smietana',
    'jugurt',
    'maslanka',
    'twarog',
    'kefir',
  ],
  komorawedlintluszczu: [
    'olejroslinny',
    'oliwazoliwek',
    'olejlniany',
    'smalec',
    'olejkokosowy',
    'wedzonki',
    'kielbasy',
    'boczek',
    'szynka',
    'kaszanka',
  ],
  komoraowocow: [
    'pomidory',
    'ogorki',
    'cukinie',
    'baklazany',
    'papryka',
    'dynia',
    'salata',
    'kalafior',
    'imbir',
    'jablka',
    'gruszki',
    'banany',
    'pomarancze',
    'sliwki',
    'brzoskwinie',
    'ananasy',
    'truskawki',
    'wisnie',
    'arbuzy',
  ],
  komorajaj: ['jaja', 'masajajeczna'],
  komoranapojow: ['soki', 'napojegazowane', 'woda'],
  komoraniskotemperaturowa: [
    'mrozonewarzywa',
    'lody',
    'mrozonepierogi',
    'mrozoneknedle',
    'mrozoneowoce',
  ],
  komorachlodniczamiesa: [
    'watrobkawieprzowa',
    'zeberka',
    'lopatka',
    'nogi',
    'antrykotwolowy',
    'boczek',
    'szynka',
  ],
};

const ListItemCustom = ({ itemObject, index, keyData }) => {
  const { columns, getImagePath, images, isClicked } = useContext(DataContext);
  const classes = useStyles();

  if (!validData[keyData]) {
  }

  const isNotValid =
    isClicked && !validData[keyData].find((key) => key == itemObject.key)
      ? true
      : false;

  const isValid =
    isClicked && validData[keyData].find((key) => key == itemObject.key)
      ? true
      : false;

  return (
    <Draggable draggableId={itemObject.key} key={itemObject.key} index={index}>
      {(provided) => (
        <ListItem
          key={itemObject.key}
          role={undefined}
          dense
          button
          ContainerComponent="li"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          {isNotValid && <div className={classes.error}></div>}
          {isValid && <div className={classes.success}></div>}

          <div style={{ display: 'flex', alignItems: 'center' }}>
            {images &&
              (itemObject.noImg ? null : (
                <img
                  style={{
                    height: 70,
                    width: 70,
                    objectFit: 'contain',
                    marginRight: 5,
                    zIndex: 1,
                  }}
                  src={getImagePath(images[`${itemObject.key}.png`])}
                  alt=""
                  crossOrigin="anonymous"
                />
              ))}
            <ListItemText
              primary={
                <div type="body2" className={`${classes.listItem}`}>
                  {itemObject.name}
                </div>
              }
              style={{ paddingLeft: '0 !import', paddingRight: '0 !import' }}
            />
          </div>
          <ListItemSecondaryAction style={{ width: '0' }}>
            {/* <IconButton
              edge="end"
              aria-label="comments"
              question-uid={itemObject.key}
            >
            </IconButton> */}
          </ListItemSecondaryAction>
        </ListItem>
      )}
    </Draggable>
  );
};

export default ListItemCustom;
