import React, { useContext } from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import { Draggable } from 'react-beautiful-dnd';
import { makeStyles } from '@material-ui/core/styles';
import DataContext from '@context/DataContext';
import storehouses from '../../../data/storehouse';

const useStyles = makeStyles((theme) => ({
  listItem: {
    position: 'relative',
    paddingLeft: '2px',
    width: '125%',

    marginLeft: '2px',
  },
  number: {
    position: 'absolute',
    top: '3px',
    left: '0',
    transform: 'translateX(-120%)',
    // backgroundColor: "#1e1c37",
    color: 'white',
    borderRadius: '50%',
    paddingTop: '4px',
    width: '20px',
    height: '20px',
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
    fontSize: '10px',
    fontWeight: 'bold',
  },
}));

const ListItemCustom = ({ itemObject, index, keyData }) => {
  const { data, getImagePath, images, isClickedAverage } =
    useContext(DataContext);
  const classes = useStyles();

  const renderFeedbackColor = (valid) => {
    let color;

    if (isClickedAverage) {
      if (keyData === valid) {
        color = 'green';
      } else {
        color = 'red';
      }
    }

    return color;
  };

  return (
    <Draggable draggableId={itemObject.key} key={itemObject.key} index={index}>
      {(provided) => (
        <ListItem
          key={itemObject.key}
          role={undefined}
          dense
          button
          ContainerComponent="li"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <div style={{ display: 'flex', alignItems: 'center' }}>
            {images &&
              (itemObject.noImg ? null : (
                <img
                  style={{
                    height: 70,
                    width: 70,
                    objectFit: 'contain',
                    marginRight: 5,
                  }}
                  src={getImagePath(images[`${itemObject.key}.png`])}
                  alt=""
                  crossOrigin="anonymous"
                />
              ))}
            <ListItemText
              primary={
                <div
                  style={{ color: renderFeedbackColor(itemObject.key) }}
                  type="body2"
                  className={`${classes.listItem}`}
                >
                  {itemObject.name}
                </div>
              }
              style={{ paddingLeft: '0 !import', paddingRight: '0 !import' }}
            />
          </div>
          <ListItemSecondaryAction style={{ width: '0' }}>
            {/* <IconButton
              edge="end"
              aria-label="comments"
              question-uid={itemObject.key}
            >
            </IconButton> */}
          </ListItemSecondaryAction>
        </ListItem>
      )}
    </Draggable>
  );
};

export default ListItemCustom;
