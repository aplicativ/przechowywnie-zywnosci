import React, { useContext } from "react";

import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import { Draggable } from "react-beautiful-dnd";
import { makeStyles } from "@material-ui/core/styles";
import arbuz from "../../../images/products/arbuzy.png";
import DataContext from "@context/DataContext";

const useStyles = makeStyles((theme) => ({
  listItem: {
    position: "relative",
    paddingLeft: "2px",
    // width: "125%",
    // width: "fit-content",
    width: "calc(100%)",
    width: "fit-content",
    // marginLeft: "2px",
    outline: "1px solid yellow",
  },
  number: {
    position: "absolute",
    top: "3px",
    left: "0",
    transform: "translateX(-120%)",
    // backgroundColor: "#1e1c37",
    color: "white",
    borderRadius: "50%",
    paddingTop: "4px",
    width: "20px",
    height: "20px",
    display: "flex",
    alignContent: "center",
    justifyContent: "center",
    fontSize: "10px",
    fontWeight: "bold",
  },
}));

const ListItemCustom = ({ itemObject, index, keyData }) => {
  const { data, getImagePath, images } = useContext(DataContext);
  const classes = useStyles();
  return (
    <Draggable draggableId={itemObject.key} key={itemObject.key} index={index}>
      {(provided) => (
        <ListItem
          key={itemObject.key}
          role={undefined}
          dense
          button
          ContainerComponent="li"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <div style={{ display: "flex", alignItems: "center" }}>
            {images &&
              (itemObject.noImg ? null : (
                <div
                  style={{
                    width: 50,
                    height: 50,
                    position: "relative",
                    marginRight: 5,
                    outline: "1px solid red",
                  }}
                >
                  <img
                    style={{
                      height: "100%",
                      width: "100%",
                      objectFit: "contain",
                      position: "absolute",
                      top: 0,
                      left: 0,
                      outline: "1px solid blue",
                    }}
                    src={getImagePath(images[`${itemObject.key}.png`])}
                    alt=""
                    crossOrigin="anonymous"
                  />
                </div>
              ))}
            <div className={`${classes.listItem}`}>{itemObject.name}</div>
            {/* <ListItemText
              primary={
                <div  className={`${classes.listItem}`}>
                  {itemObject.name}
                </div>
              }
              style={{ paddingLeft: "0 !import", paddingRight: "0 !import" }}
            /> */}
          </div>
          <ListItemSecondaryAction
            style={{ width: "0", outline: "1px solid pink" }}
          >
            {/* <IconButton
              edge="end"
              aria-label="comments"
              question-uid={itemObject.key}
            >
            </IconButton> */}
          </ListItemSecondaryAction>
        </ListItem>
      )}
    </Draggable>
  );
};

export default ListItemCustom;
