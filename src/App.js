import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { Button, Dialog } from '@material-ui/core';
import './index.css';

import CssBaseline from '@material-ui/core/CssBaseline';
import BottomNav from './components/BottomNavigation';
import DataContext from './context/DataContext';
import ModalContext from './context/ModalContext';

import Header from '@components/Header';
import MainNavigation from '@components/MainNavigation';
import Content from '@components/Content';
import KnowledgeBase from './components/KnowledgeBase';

import Modal from '@components/Modal';
import Alert from '@components/Alert';

import initialState from '@data/initialState';
import { easyList, averageList } from '@data/features';

import useStyles from './styles';
import { SnackbarProvider } from 'notistack';
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';

import StepsPdf from '@components/StepsPdf';

import DragDropTransfer from '@components/DragDropTransfer';
import { updateCookieState, getCookie, isValidState } from '@lib/cookies';

import printSteps from '@lib/pdf';
import Instruction from '@components/Instruction';
import Recommendation from '@components/Recommendation';
// import arbuz from "./images";

import {
  setLocalStorage,
  getLocalStorage,
  isValidLocalStorage,
} from '@lib/localStorage';

const theme = createTheme({
  palette: {
    primary: {
      main: '#1e1c37',
    },
    secondary: {
      main: '#ec8b00',
    },
  },
  typography: {
    fontFamily: ['OpenSans', 'sans-serif'].join(','),
    h2: {
      fontSize: '24px',
      fontWeight: 'bold',
      marginBottom: '10px',
      textAlign: 'center',
      fontFamily: ['Oswald', 'sans-serif'].join(','),
    },
  },
  overrides: {
    MuiTooltip: {
      popper: {
        zIndex: '999999 !important',
      },
    },
  },
});

const modalState = {
  isOpen: false,
  type: null,
  title: null,
  text: null,
};

const alertState = {
  isOpen: false,
  type: null,
  text: null,
};

const App = ({ getImagePath }) => {
  const classes = useStyles();
  const [state, setState] = useState(initialState);
  const [isFullscreen, setIsFullscreen] = useState(false);
  const { level } = state;
  const { [level]: data } = state;
  const [isOpenNavigation, setIsOpenNavigation] = useState(true);
  const [modalParams, setModalParams] = useState(modalState);
  const [alertParams, setAlertParams] = useState(alertState);
  const [easyColumns, setEasyColumns] = useState(easyList);
  const [averageColumns, setAverageColumns] = useState(averageList);
  const appID = 'przechowywane-zywnosci-v2';
  const [isInstructionOpen, setIsInstructionOpen] = useState(false);
  const [isRecommendationOpen, setIsRecommendationOpen] = useState(false);
  const [isKnowledgeBaseOpen, setIsKnowledgeBaseOpen] = useState(false);
  const [images, setImages] = useState(null);
  const [isClicked, setIsClicked] = useState(false);
  const [isClickedAverage, setIsClickedAverage] = useState(false);

  let columns = level === 'easy' ? easyColumns : averageColumns;
  let setColumns = level === 'easy' ? setEasyColumns : setAverageColumns;

  useEffect(() => {
    if (isFullscreen) {
      setIsOpenNavigation(true);
    }
  }, [isFullscreen]);

  useEffect(() => {
    if (isValidLocalStorage(appID)) {
      setState(JSON.parse(getLocalStorage(appID)));
    }
    if (isValidLocalStorage(appID + 'easy')) {
      setEasyColumns(JSON.parse(getLocalStorage(appID + 'easy')));
    }
    if (isValidLocalStorage(appID + 'average')) {
      setAverageColumns(JSON.parse(getLocalStorage(appID + 'average')));
    }
  }, []);

  useEffect(() => {
    function importAll(r) {
      let images = {};
      r.keys().forEach((item, index) => {
        images[item.replace('./', '')] = r(item);
      });
      return images;
    }
    setImages(
      importAll(
        require.context('./images/products', false, /\.(png|jpe?g|svg)$/)
      )
    );
  }, []);

  return (
    <MuiThemeProvider theme={theme}>
      <SnackbarProvider
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
      >
        <ModalContext.Provider
          value={{
            modalParams,
            setModalParams,
            alertParams,
            setAlertParams,
            isInstructionOpen,
            setIsInstructionOpen,
            isRecommendationOpen,
            setIsRecommendationOpen,
            isKnowledgeBaseOpen,
            setIsKnowledgeBaseOpen,
          }}
        >
          <DataContext.Provider
            value={{
              images,
              printSteps,
              state,
              data,
              setState,
              level,
              getImagePath,
              isFullscreen,
              setIsFullscreen,
              columns,
              setColumns,
              appID,
              setAverageColumns,
              averageColumns,
              setEasyColumns,
              easyColumns,
              isClicked,
              setIsClicked,
              isClickedAverage,
              setIsClickedAverage,
            }}
          >
            <div
              className={clsx(`${classes.root}`, {
                [classes.fullScreen]: isFullscreen,
              })}
            >
              <CssBaseline />
              <DragDropTransfer>
                <Header
                  isOpenNavigation={isOpenNavigation}
                  setIsOpenNavigation={setIsOpenNavigation}
                />
                <MainNavigation
                  isOpenNavigation={isOpenNavigation}
                  setIsOpenNavigation={setIsOpenNavigation}
                />

                <main
                  className={clsx(classes.content, {
                    [classes.contentShift]: isOpenNavigation,
                  })}
                >
                  <div className={classes.drawerHeader} />
                  <div id="app" className={classes.task}>
                    <Content />
                  </div>

                  <footer
                    className={clsx(classes.footer, {
                      [classes.contentShift]: isOpenNavigation,
                    })}
                  >
                    <BottomNav />
                  </footer>
                </main>
              </DragDropTransfer>
            </div>
            <Modal />
            <Alert />
            <StepsPdf />
            <Instruction />
            <Recommendation />
            <KnowledgeBase />
          </DataContext.Provider>
        </ModalContext.Provider>
      </SnackbarProvider>
    </MuiThemeProvider>
  );
};

export default App;
