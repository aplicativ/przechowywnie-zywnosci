const easyList = {
  menu: {
    id: "menu",
    list: [
      // Artykułów suchych
      { key: "maka", name: "Mąka pszenna w opakowaniu papierowym" },
      { key: "mrozonewarzywa", name: "Mrożone warzywa" },
      { key: "lody", name: "Lody" },
      { key: "kasza", name: "Kasza w opakowaniu kartonowym" },
      { key: "buraki", name: "Buraki świeże" },
      { key: "dynia", name: "Dynia świeża" },
      { key: "salata", name: "Sałata lodowa" },
      { key: "kalafior", name: "Kalafior świeży" },
      { key: "sol", name: "Sól" },
      { key: "przyprawy", name: "Przyprawy w opakowaniach jednostkowych" },
      { key: "wodka", name: "Wódka" },
      { key: "makaron", name: "Makaron w opakowaniu kartonowym" },
      { key: "pieczywo", name: "Pieczywo świeżo pieczone" },
      { key: "ryz", name: "Ryż w opakowaniu papierowym" },
      { key: "wodkalikier", name: "Wódka słodka typu likier" },

      // Winni wódek
      { key: "winaczerwone", name: "Wina czerwone" },

      // Ziemniaków i warzyw korzeniowych
      { key: "ziemniaki", name: "Ziemniaki" },
      { key: "imbir", name: "Imbir świeży" },
      { key: "cukier", name: "Cukier kryształ" },

      // Kiszonek

      { key: "czosnek", name: "Czosnek świeży" },
      { key: "cebula", name: "Cebula świeża" },
      { key: "sledzie", name: "Szproty marynowane w puszce" },
      { key: "winabiale", name: "Wina białe" },
      { key: "whisky", name: "Wódka typu whisky" },
      // Komora chłodnicza mięsia
      { key: "zeberka", name: "Żeberka wieprzowe świeże" },
      { key: "dzemy", name: "Dżemy w słoikach" },
      { key: "kawa", name: "Kawa ziarnista w opakowaniu foliowym" },
      { key: "ciasto", name: "Ciasto czekoladowe świeżo pieczone" },
      { key: "ciasteczka", name: "Ciastka kruche świeżo pieczone" },
      { key: "babeczki", name: "Babeczki świeżo pieczone" },
      { key: "nogi", name: "Nogi wieprzowe świeże" },

      // Komora chłodnicza drobiu

      // Komora ryb i owoców morza
      { key: "losos", name: "Łosoś świeży" },
      { key: "brzoskwinie", name: "Brzoskwinie świeże" },
      { key: "ananasy", name: "Ananasy świeże" },
      { key: "truskawki", name: "Truskawki świeże" },
      { key: "wisnie", name: "Wiśnie świeże" },
      { key: "dorsz", name: "Dorsz świeży" },

      // Komora chłodnicza nabiału
      { key: "mlekoswieze", name: "Mleko świeże" },
      { key: "jugurt", name: "Jogurt" },
      { key: "maslanka", name: "Maślanka" },
      { key: "twarog", name: "Twaróg" },
      { key: "sos", name: "Sos (wyrób garmażeryjny pasteryzowany w butelce)" },
      {
        key: "golabki",
        name: "Gołąbki (wyrób garmażeryjny pasteryzowany w słoiku) ",
      },
      { key: "kefir", name: "Kefir" },

      // Komora chłodnicza wędlin i tłuszczów
      { key: "olejroslinny", name: "Olej słonecznikowy" },
      { key: "napojegazowane", name: "Napoje gazowane" },
      { key: "woda", name: "Woda mineralna butelkowana" },
      { key: "oliwazoliwek", name: "Oliwa z oliwek" },
      { key: "herbata", name: "Herbata sypka w opakowaniu kartonowym" },
      { key: "lopatka", name: "Łopatka wołowa świeża" },
      { key: "mlekouht", name: "Mleko UHT" },
      { key: "smietana", name: "Śmietana" },
      { key: "watrobkawieprzowa", name: "Wątroba cielęca świeża" },
      { key: "olejlniany", name: "Olej lniany" },

      // Komora chłodnicza owoców i warzyw nietrwałych
      { key: "pomidory", name: "Pomidory świeże" },
      { key: "ogorki", name: "Ogórki zielone" },
      { key: "boczek", name: "Boczek wieprzowy świeży" },
      { key: "szynka", name: "Szynka wieprzowa świeża" },
      { key: "cukinie", name: "Cukinie świeże" },
      { key: "baklazany", name: "Bakłażany świeże" },
      { key: "olejkokosowy", name: "Olej kokosowy" },
      { key: "wedzonki", name: "Wędzonki" },
      { key: "kaszanka", name: "Kaszanka" },
      { key: "papryka", name: "Papryka świeża" },
      { key: "jablka", name: "Jabłka świeże" },
      { key: "gruszki", name: "Gruszki świeże" },
      { key: "banany", name: "Banany świeże" },
      { key: "pomarancze", name: "Pomarańcze świeże" },
      { key: "smalec", name: "Smalec" },
      { key: "kielbasy", name: "Kiełbasy" },
      { key: "sliwki", name: "Śliwki świeże" },

      // Komora chłodnicza jaj
      { key: "jaja", name: "Jaja kurze" },

      // Komora chłodnicza piw i napojów
      { key: "soki", name: "Soki pasteryzowane w butelkach" },
      { key: "krewetki", name: "Krewetki świeże" },
      { key: "kalmary", name: "Kalmary świeże" },

      // Komora niskotemperaturowa
      { key: "mrozonepierogi", name: "Mrożone pierogi" },
      { key: "masajajeczna", name: "Masa jajeczna UHT w kartonie" },
      {
        key: "pesto",
        name: "Pesto (wyrób garmażeryjny pasteryzowany w słoiku)",
      },
      { key: "mrozoneknedle", name: "Mrożone knedle" },
      { key: "mrozoneowoce", name: "Mrożone owoce" },

      { key: "antrykotwolowy", name: "Antrykot wołowy świeży" },
      { key: "arbuzy", name: "Arbuzy świeże" },
      { key: "tusza", name: "Świeża tusza drobiowa" },
      // // Artykułów suchych
      // { key: "maka", name: "Mąka" },
      // { key: "kasza", name: "Kasza" },
      // { key: "cukier", name: "Cukier" },
      // { key: "sol", name: "Sól" },
      // { key: "przyprawy", name: "Przyprawy" },
      // { key: "makaron", name: "Makaron" },
      // { key: "pieczywo", name: "Pieczywo" },
      // { key: "ryz", name: "Ryż" },
      // { key: "dzemy", name: "Dżemy" },
      // { key: "kawa", name: "Kawa" },
      // { key: "herbata", name: "Herbata" },
      // { key: "ciasto", name: "Ciasto" },
      // { key: "ciasteczka", name: "Ciasteczka" },
      // { key: "babeczki", name: "Babeczki" },

      // // Winni wódek
      // { key: "winaczerwone", name: "Wina czerwone" },
      // { key: "winarozowe", name: "Wina różowe" },
      // { key: "winabiale", name: "Wina białe" },
      // { key: "wodka", name: "Wódka" },
      // { key: "whisky", name: "Whisky" },

      // // Ziemniaków i warzyw korzeniowych
      // { key: "ziemniaki", name: "Ziemniaki" },
      // { key: "buraki", name: "Buraki" },
      // { key: "marchewka", name: "Marchew" },
      // { key: "pietruszka", name: "Pietruszka" },
      // { key: "seler", name: "Seler" },
      // { key: "czosnek", name: "Czosnek" },
      // { key: "cebula", name: "Cebula" },

      // // Kiszonek
      // { key: "kapustakiszona", name: "Kapusta kiszona" },
      // { key: "sledzie", name: "Śledzie" },
      // { key: "ogorkikiszone", name: "Ogórki kiszone" },
      // { key: "kiszonamarchewka", name: "Kiszona marchew" },
      // { key: "zupa", name: "Zupa (wyrób garmażeryjny)" },
      // { key: "sos", name: "Sos (wyrób garmażeryjny)" },
      // { key: "golabki", name: "Gołąbki w słoiku" },

      // // Komora chłodnicza mięsia
      // { key: "watrobkawieprzowa", name: "Wątroba wieprzowa" },
      // { key: "pluckawolowe", name: "Płucka wołowe" },
      // { key: "zeberka", name: "Żeberka" },
      // { key: "lopatka", name: "Łopatka" },
      // { key: "nogi", name: "Nogi" },
      // { key: "mostek", name: "Mostek" },

      // // Komora chłodnicza drobiu
      // { key: "skrzydelka", name: "Skrzydła kurczaka" },
      // { key: "szyja", name: "Szyja" },
      // { key: "watrabkadrobiowa", name: "Wątroba drobiowa" },

      // // Komora ryb i owoców morza
      // { key: "losos", name: "Łosoś" },
      // { key: "dorsz", name: "Dorsz" },
      // { key: "krewetki", name: "Krewetki" },
      // { key: "kalmary", name: "Kalmary" },

      // // Komora chłodnicza nabiału
      // { key: "mlekoswieze", name: "Mleko świeże" },
      // { key: "mlekouht", name: "Mleko UHT" },
      // { key: "smietana", name: "Śmietana" },
      // { key: "jugurt", name: "Jogurt" },
      // { key: "maslanka", name: "Maślanka" },
      // { key: "twarog", name: "Twaróg" },
      // { key: "kefir", name: "Kefir" },

      // // Komora chłodnicza wędlin i tłuszczów
      // { key: "olejroslinny", name: "Olej roślinny" },
      // { key: "oliwazoliwek", name: "Oliwa z oliwek" },
      // { key: "olejlniany", name: "Olej lniany" },
      // { key: "smalec", name: "Smalec" },
      // { key: "olejkokosowy", name: "Olej kokosowy" },
      // { key: "wedzonki", name: "Wędzonki" },
      // { key: "kaszanka", name: "Kaszanka" },
      // { key: "kielbasy", name: "Kiełbasy" },
      // { key: "boczek", name: "Boczek" },
      // { key: "szynka", name: "Szynka" },

      // // Komora chłodnicza owoców i warzyw nietrwałych
      // { key: "pomidory", name: "Pomidory" },
      // { key: "ogorki", name: "Ogórki" },
      // { key: "cukinie", name: "Cukinie" },
      // { key: "baklazany", name: "Bakłażany" },
      // { key: "papryka", name: "Papryka" },
      // { key: "dynia", name: "Dynia" },
      // { key: "salata", name: "Sałata" },
      // { key: "kalafior", name: "Kalafior" },
      // { key: "imbir", name: "Imbir" },
      // { key: "jablka", name: "Jabłka" },
      // { key: "gruszki", name: "Gruszki" },
      // { key: "banany", name: "Banany" },
      // { key: "pomarancze", name: "Pomarańcze" },
      // { key: "grejpfruty", name: "Grejpfruty" },
      // { key: "sliwki", name: "Śliwki" },
      // { key: "brzoskwinie", name: "Brzoskwinie" },
      // { key: "ananasy", name: "Ananasy" },
      // { key: "truskawki", name: "Truskawki" },
      // { key: "wisnie", name: "Wiśnie" },

      // // Komora chłodnicza jaj
      // { key: "jaja", name: "Jaja" },
      // { key: "masajajeczna", name: "Masa jajeczna UHT" },
      // { key: "pastajajecza", name: "Pasta jajeczna" },

      // // Komora chłodnicza piw i napojów
      // { key: "soki", name: "Soki" },
      // { key: "napojegazowane", name: "Napoje gazowane" },
      // { key: "woda", name: "Woda" },

      // // Komora niskotemperaturowa
      // { key: "mrozonewarzywa", name: "Mrożone warzywa" },
      // { key: "lody", name: "Lody" },
      // { key: "mrozonepierogi", name: "Mrożone pierogi" },
      // { key: "mrozoneknedle", name: "Mrożone knedle" },
      // { key: "mrozoneowoce", name: "Mrożone owoce" },
    ],
  },
  artykulowsuchych: {
    id: "artykulowsuchych",
    list: [],
  },
  winiwodek: {
    id: "winiwodek",
    list: [],
  },
  ziemniakowwarzywkorzeniowych: {
    id: "ziemniakowwarzywkorzeniowych",
    list: [],
  },
  kiszonek: {
    id: "kiszonek",
    list: [],
  },
  komorachlodniczamiesa: {
    id: "komorachlodniczamiesa",
    list: [],
  },
  komorachlodniczadrobiu: {
    id: "komorachlodniczadrobiu",
    list: [],
  },
  komorarybiowocowmorza: {
    id: "komorarybiowocowmorza",
    list: [],
  },
  komoranabialu: {
    id: "komoranabialu",
    list: [],
  },

  komorawedlintluszczu: {
    id: "komorawedlintluszczu",
    list: [],
  },

  komoraowocow: {
    id: "komoraowocow",
    list: [],
  },

  komorajaj: {
    id: "komorajaj",
    list: [],
  },

  komoranapojow: {
    id: "komoranapojow",
    list: [],
  },
  komoraniskotemperaturowa: {
    id: "komoraniskotemperaturowa",
    list: [],
  },
};

const averageList = {
  menu: {
    id: "menu",
    list: [
      { key: "magazynsuchy", name: "Magazyn artykułów suchych", noImg: true },
      { key: "magazynwin", name: "Magazyn win i wódek", noImg: true },
      {
        key: "magazynziemniakowwarzyw",
        name: "Magazyn ziemniaków i warzyw korzeniowych",
        noImg: true,
      },
      { key: "magazynkiszonek", name: "Magazyn kiszonek", noImg: true },
      { key: "magazynmiesa", name: "Komora chłodnicza mięsa", noImg: true },
      { key: "magazyndrobiu", name: "Komora chłodnicza drobiu", noImg: true },
      {
        key: "magazynryb",
        name: "Komora chłodnicza ryb i owoców morza",
        noImg: true,
      },
      { key: "magazynnabialu", name: "Komora chłodnicza nabiału", noImg: true },
      {
        key: "magazynwedlin",
        name: "Komora chłodnicza wędlin i tłuszczów",
        noImg: true,
      },
      {
        key: "magazynwarzywchlodnia",
        name: "Komora chłodnicza owoców i warzyw nietrwałych",
        noImg: true,
      },
      {
        key: "magazynpiw",
        name: "Komora chłodnicza piw i napojów",
        noImg: true,
      },
      {
        key: "magazyntemperatura",
        name: "Komora niskotemperaturowa",
        noImg: true,
      },
      { key: "magazynjaj", name: "Komora chłodnicza jaj", noImg: true },
    ],
  },
  magazynsuchy: {
    id: "magazynsuchy",
    list: [],
  },
  magazynwin: {
    id: "magazynwin",
    list: [],
  },
  magazynziemniakowwarzyw: {
    id: "magazynziemniakowwarzyw",
    list: [],
  },
  magazynkiszonek: {
    id: "magazynkiszonek",
    list: [],
  },
  magazynmiesa: {
    id: "magazynmiesa",
    list: [],
  },
  magazyndrobiu: {
    id: "magazyndrobiu",
    list: [],
  },
  magazynryb: {
    id: "magazynryb",
    list: [],
  },
  magazynnabialu: {
    id: "magazynnabialu",
    list: [],
  },
  magazynwedlin: {
    id: "magazynwedlin",
    list: [],
  },
  magazynwarzywchlodnia: {
    id: "magazynwarzywchlodnia",
    list: [],
  },
  magazynpiw: {
    id: "magazynpiw",
    list: [],
  },
  magazyntemperatura: {
    id: "magazyntemperatura",
    list: [],
  },
  magazynjaj: {
    id: "magazynjaj",
    list: [],
  },
};

export { easyList, averageList };

// { key: "magazynsuchy", name: "Magazyn artykułów suchych" },
// { key: "magazynwin", name: "Magazyn win i wódek" },
// { key: "magazynziemniakowwarzyw", name: "Magazyn ziemniaków i warzyw korzeniowych" },
// { key: "magazynkiszonek", name: "Magazyn kiszonek" },
// { key: "magazynmiesa", name: "Komora chłodnicza mięsa" },
// { key: "magazyndrobiu", name: "Komora chłodnicza drobiu" },
// { key: "magazynryb", name: "Komora chłodnicza ryb i owoców morza" },
// { key: "magazynnabialu", name: "Komora chłodnicza nabiału" },
// { key: "magazynwedlin", name: "Komora chłodnicza wędlin i tłuszczów" },
// { key: "magazynwarzywchlodnia", name: "Komora chłodnicza owoców i warzyw nietrwałych" },
// { key: "magazynpiw", name: "Komora chłodnicza piw i napojów" },
// { key: "magazyntemperatura", name: "Komora niskotemperaturowa" },
