import kiszonki from "./../images/kiszonki.png";
import mieso from "./../images/komora-chlodnicza-mieso.png";
import warzywnietrwalych from "./../images/komora-chłodnicza-owocow-i-warzyw-nietrwalych.png";
import nabial from "./../images/komory-chłodnicze-nabial.png";
import suche from "./../images/magazyn-artykułów-suchych.png";
import ziemniaki from "./../images/magazyn-na-ziemniaki.png";
import wina from "./../images/magazyn-win-i-wodek.png";
import wedliny from "./../images/magazyn-wedlin.png";
import jaja from "./../images/magazyn-jaj.png";
import niskoTemp from "./../images/magazyn-niskotemp.png";
import piwa from "./../images/magazyn-piwa.png";
import ryby from "./../images/ryby.png";
import drob from "./../images/drob.png";

const initialState = {
  magazynsuchy: {
    image: suche,
    validFeatures: ["wilg60", "temp15+18", "osw-dop-nat", "went-mech"],
  },
  magazynwin: {
    image: wina,
    validFeatures: ["wilg60-80", "temp10+18", "osw-sztuczne", "went-mech"],
  },
  magazynziemniakowwarzyw: {
    image: ziemniaki,
    validFeatures: [
      "wilg85-90",
      "temp6-10",
      "osw-sztuczne",
      "went-mech",
      "osw-jesien-bielony",
    ],
  },
  magazynkiszonek: {
    image: kiszonki,
    validFeatures: ["wilg70-80", "temp+6+15", "osw-sztuczne", "went-mech"],
  },
  magazynmiesa: {
    image: mieso,
    validFeatures: ["wilg70-90", "temp0+4", "osw-sztuczne"],
  },
  magazyndrobiu: {
    image: drob,
    validFeatures: ["wilg70-90", "temp0+4", "osw-sztuczne"],
  },
  magazynryb: {
    image: ryby,
    validFeatures: ["wilg90-93", "temp-2+2", "osw-sztuczne"],
  },
  magazynnabialu: {
    image: nabial,
    validFeatures: ["wilg80-85", "temp+2+4", "osw-sztuczne"],
  },
  magazynwedlin: {
    image: wedliny,
    validFeatures: ["wilg80", "temp0+2", "osw-sztuczne"],
  },
  magazynwarzywchlodnia: {
    image: warzywnietrwalych,
    validFeatures: ["wilg80-85", "temp4+8", "osw-sztuczne"],
  },
  magazynpiw: {
    image: piwa,
    validFeatures: ["wilg80-85", "temp4+8", "osw-sztuczne"],
  },
  magazyntemperatura: {
    image: niskoTemp,
    validFeatures: ["wilg80-90", "temp-22-18", "osw-sztuczne"],
  },
  magazynjaj: {
    image: jaja,
    validFeatures: ["wilg50", "temp4+5", "osw-sztuczne"],
  },
};

export default initialState;
