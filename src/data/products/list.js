import { ReactComponent as skirtSVG } from "./img/skirt.svg";
import { ReactComponent as trousersSVG } from "./img/trousers.svg";
import { ReactComponent as blouseSVG } from "./img/blouse.svg";
import { ReactComponent as dressSVG } from "./img/dress.svg";
import { ReactComponent as chaquetaSVG } from "./img/chaqueta.svg";



const ProductsList = [
    {
        key: "skirt",
        name: "Spódnica",
        level: ["all"],
        description: "Klasyczna prosta spódnica charakteryzuje się dyskrecją, elegancją i wspaniale odpowiada stylowi klasycznemu. Zazwyczaj, jest ona uszyta w delikatnych, niejaskrawych kolorach, nie ma żadnych detali dekoracyjnych.",
        validFabrics: ["linen", "cotton", "jeans", "velvet"],
        validType: ["woman"],
        image: skirtSVG,
    },
    {
        key: "trousers",
        name: "Spodnie",
        level: ["average","hard"],
        description: "Spodnie – wierzchnia część ubrania osłaniająca ciało od pasa w dół. Noszone są zarówno jako element dwu- lub trzyczęściowego garnituru, jak i samodzielnie.  Współcześnie są elementem ubrań zarówno kobiecych, jak i męskich. Spodnie szyje się z różnych materiałów zależnie od przeznaczenia. Najczęściej wykonywane są z tkanin lub dzianin. Rodzaje spodni ze względu na długość: Spodnie długie, zakrywające kostki, w tym wykonaniu są elementem bardziej eleganckich ubrań. W ubraniach tych bardzo ważna jest długość spodni. Spodnie powinny być tak długie, żeby nie zaginały się na butach. Przy postawie wyprostowanej z tyłu powinny prawie sięgać obcasa, a z przodu układać się na bucie nie tworząc zagięć, najwyżej lekkie zafalowanie. Nogawka może być nieco dłuższa z tyłu niż z przodu, różnica do 2–3 cm. W czasie normalnego wykroku nogawka nie powinna odsłaniać skarpetki. Spodnie krótkie to spodnie odkrywające łydkę lub krótsze. Noszone są najczęściej latem, ze względu na wygodę i większe poczucie komfortu cieplnego. W tym wykonaniu są uważane za mało eleganckie i niektórzy pracodawcy zabraniają noszenia krótkich spodni przez pracowników, nawet w upalne dni. ",
        validFabrics: ["linen", "jeans", "cotton"],
        validType: ["woman", "man"],
        image: trousersSVG
    },
    {
        key: "blouse",
        name: "Bluzka",
        level: ["easy"],
        description: "Spodnie – wierzchnia część ubrania osłaniająca ciało od pasa w dół. Noszone są zarówno jako element dwu- lub trzyczęściowego garnituru, jak i samodzielnie.  Współcześnie są elementem ubrań zarówno kobiecych, jak i męskich. Spodnie szyje się z różnych materiałów zależnie od przeznaczenia. Najczęściej wykonywane są z tkanin lub dzianin. Rodzaje spodni ze względu na długość: Spodnie długie, zakrywające kostki, w tym wykonaniu są elementem bardziej eleganckich ubrań. W ubraniach tych bardzo ważna jest długość spodni. Spodnie powinny być tak długie, żeby nie zaginały się na butach. Przy postawie wyprostowanej z tyłu powinny prawie sięgać obcasa, a z przodu układać się na bucie nie tworząc zagięć, najwyżej lekkie zafalowanie. Nogawka może być nieco dłuższa z tyłu niż z przodu, różnica do 2–3 cm. W czasie normalnego wykroku nogawka nie powinna odsłaniać skarpetki. Spodnie krótkie to spodnie odkrywające łydkę lub krótsze. Noszone są najczęściej latem, ze względu na wygodę i większe poczucie komfortu cieplnego. W tym wykonaniu są uważane za mało eleganckie i niektórzy pracodawcy zabraniają noszenia krótkich spodni przez pracowników, nawet w upalne dni. ",
        validFabrics: ["silk", "lenin", "cotton"],
        validType: ["woman", "man"],
        image: blouseSVG
    },
    {
        key: "dress",
        name: "Sukienka",
        level: ["average"],
        description: "Suknia, sukienka – wierzchni jednoczęściowy strój, okrywający tułów i nogi, często także ręce. Sukienka to najbardziej charakterystyczny element damskiej garderoby. Choć przez lata jej fason mocno ewoluował, sukienka nadal jest prawdziwym atrybutem kobiecości. Może być noszona na wiele okazji, potrafi być wyznacznikiem elegancji lub wakacyjnej swobody. Sukienki są hitem każdego sezonu. Ich popularność nie słabnie od lat, mimo że od dawna konkurują ze spodniami jako bazowy element garderoby, często to mała czarna lub klasyczna, rozszerzana sukienka stanowi podstawę stylizacji. Każda kobieta, bez względu na figurę, może znaleźć dla siebie odpowiedni model, w którym podkreśli atuty swojej sylwetki i zatuszuje niedoskonałości. Dzięki zmianie dodatków ulubiony model sukienki możemy założyć zarówno na dzień, jak i na wieczór. Oferta modnych sukienek w każdym sezonie jest ogromna, różnorodność fasonów zaskakuje. Od bawełnianych, letnich modeli, przez koktajlowe, ołówkowe i długie balowe suknie. ",
        validFabrics: ["silk", "lenin", "cotton"],
        validType: ["woman"],
        image: dressSVG
    },
    {
        key: "chaqueta",
        name: "Żakiet",
        level: ["hard"],
        description: "Występuje w  formie ubrania wierzchniego i mogą być noszone jako część kostiumu lub zestawione z innymi elementami, np. jeansami. Najbardziej klasyczne fasony są zapinane na guziki, podkreślają talię i występują w stonowanych kolorach, np. granatowym lub czarnym.   Żakiet zagościł na stałe w damskiej garderobie i  zestawiany jest ze spódnicą czy sukienką, ale także ze spodniami. Obecnie jest podstawą zarówno formalnego jak i codziennego stroju, a fasony, które kiedyś zarezerwowane były wyłącznie na specjalne okazje,  dziś  łączone są  z poszarpanymi jeansami i szortami.",
        validFabrics: ["silk", "lenin", "cotton", "velvet"],
        validType: ["woman"],
        image: chaquetaSVG
    },

]

export default ProductsList;

