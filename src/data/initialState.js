const initialState = {
    level: "easy",

    easy: {
        bannedFeatures: [],
    },
    average: {
        magazynsuchy: [],
        magazynwin: [],
        magazynziemniakowwarzyw: [],
        magazynkiszonek: [],
        magazynmiesa: [],
        magazyndrobiu: [],
        magazynryb: [],
        magazynnabialu: [],
        magazynwedlin: [],
        magazynwarzywchlodnia: [],
        magazynpiw: [],
        magazyntemperatura: [],
        magazynjaj: [],
        bannedFeatures: ['type'],
    },
    hard: {
        bannedFeatures: [],
    }

}


export default initialState;

